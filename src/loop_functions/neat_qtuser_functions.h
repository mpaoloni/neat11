#ifndef NEAT_QTUSER_FUNCTIONS_H
#define NEAT_QTUSER_FUNCTIONS_H

#include <argos3/plugins/simulator/visualizations/qt-opengl/qtopengl_user_functions.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>

using namespace argos;
class NeatLoopFunctions;

class NeatQTUserFunctions : public CQTOpenGLUserFunctions {

public:
  NeatQTUserFunctions();
  virtual ~NeatQTUserFunctions() {}
  virtual void DrawInWorld();

private:
  void drawWaypoints(const std::vector<CVector3>& waypoints);
  NeatLoopFunctions& mNeatLoopFunctions;
};

#endif
