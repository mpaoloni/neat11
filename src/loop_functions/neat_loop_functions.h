#ifndef NEAT_LOOP_FUNCTIONS_H
#define NEAT_LOOP_FUNCTIONS_H

/* ARGoS-related headers */
#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/utility/math/rng.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>

/* Neat-related headers */
#include "lib/libneat/neat.h"

#include "controller/neat_controller.h"

#include "argos_config.h"

#define NUM_LIGHT_SENSORS 24
#define NUM_PROXIMITY_SENSORS 24
#define NUM_WHEELS 2

#ifdef OBSTACLE_AVOIDANCE
# define SENSOR_LAYER_SIZE (NUM_LIGHT_SENSORS + NUM_PROXIMITY_SENSORS)
#else
# define SENSOR_LAYER_SIZE (NUM_LIGHT_SENSORS)
#endif
#define OUTPUT_LAYER_SIZE (NUM_WHEELS)

using namespace argos;

/* A set of hook functions to customize an experimental run */
class NeatLoopFunctions : public CLoopFunctions {

public:
  NeatLoopFunctions();
  virtual ~NeatLoopFunctions();
  virtual void Init(TConfigurationNode& t_node);
  virtual void PostStep();
  virtual void Reset();

  /* Called by the evolutionary algorithm to set the current trial */
  inline void setTrial(size_t trial) { mCurrentTrial = trial % NUM_TRIALS; }
  inline const std::vector<CVector3>& getWaypoints() const { return mWaypoints; }

  /* Calculates the performance of the robot in a trial */
  Real performance();

  /* Keep track of the points where the robot has been to draw a line */
  std::vector<CVector3> mWaypoints;

private:
  CFootBotEntity* mFootBot;
  NeatController* mController;

  /* The initial setup for each trial */
  struct SInitSetup {
    CVector3 Position;
    CQuaternion Orientation;
  };
  std::vector<SInitSetup> mVecInitSetup;

  unsigned long mCurrentTrial;
  CRandom::CRNG* mRNG;
};

#endif /* NEAT_LOOP_FUNCTIONS_H */
