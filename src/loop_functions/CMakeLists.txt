link_directories(${CMAKE_BINARY_DIR}/controller)

add_library(neat_loop_functions SHARED
  neat_loop_functions.cc
  neat_qtuser_functions.h
  neat_qtuser_functions.cc)

target_link_libraries(neat_loop_functions
  footbot_neat
  argos3core_simulator
  argos3plugin_simulator_dynamics2d
  argos3plugin_simulator_entities
  argos3plugin_simulator_footbot
  argos3plugin_simulator_genericrobot
  argos3plugin_simulator_media
  argos3plugin_simulator_qtopengl
  ${LIBNEAT_LIBRARIES}
  ${ARGOS_QTOPENGL_LIBRARIES})
