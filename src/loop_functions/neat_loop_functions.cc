#include "neat_loop_functions.h"

#include "argos_config.h"

NeatLoopFunctions::NeatLoopFunctions()
    : mCurrentTrial(0),
      mVecInitSetup(NUM_TRIALS),
      mFootBot(NULL),
      mController(NULL),
      mRNG(NULL) {}

NeatLoopFunctions::~NeatLoopFunctions() {}

/* Create robot, get controller */
void NeatLoopFunctions::Init(TConfigurationNode& t_node) {
  mRNG = CRandom::CreateRNG("argos");

  /* Create the little robot-thing */
  mFootBot = new CFootBotEntity("fb",  // Entity id
                                "fnn"  // Controller id as set in the XML
                                );
  AddEntity(*mFootBot);

  /* Cast 'CCI_Controller' to our derived class */
  mController = &dynamic_cast<NeatController&>(mFootBot->GetControllableEntity().GetController());

  /* Create the initial setup for each trial (more than 5) */
  CRadians cOrient;
  for (unsigned i = 0; i < NUM_TRIALS; i++) {
    /* Set position */
    mVecInitSetup[i].Position.FromSphericalCoords(
        4.5f,                                            // Distance from origin
        CRadians::PI_OVER_TWO,                           // Angle with Z axis
        static_cast<Real>(i + 1) * CRadians::PI / 12.0f  // Rotation around Z
        );

    /* Set orientation */
    cOrient = mRNG->Uniform(CRadians::UNSIGNED_RANGE);
    mVecInitSetup[i].Orientation.FromEulerAngles(cOrient,         // Rotation around Z
                                                 CRadians::ZERO,  // Rotation around Y
                                                 CRadians::ZERO   // Rotation around X
                                                 );
    LOGERR << "[!] Robot position[" << i << "] = " << mVecInitSetup[i].Position
           << ", orientation[" << i << "] = " << mVecInitSetup[i].Orientation << '\n';
  }

  /* Process trial information, if any */
  try {
    GetNodeAttribute(t_node, "trial", mCurrentTrial);
    LOGERR << "[!] Trial = " << mCurrentTrial
           << ", position = " << mVecInitSetup[mCurrentTrial].Position
           << ", orientation = " << mVecInitSetup[mCurrentTrial].Orientation << '\n';
    Reset();
  } catch (CARGoSException& ex) {
  }
}

void NeatLoopFunctions::PostStep() {
  // LOG << mFootBot->GetEmbodiedEntity().GetOriginAnchor().Position << '\n';
  mWaypoints.push_back(mFootBot->GetEmbodiedEntity().GetOriginAnchor().Position);
}

void NeatLoopFunctions::Reset() {
  /* Move robot to the initial position corresponding to the current trial */
  if (!MoveEntity(mFootBot->GetEmbodiedEntity(),             // Move the body of the robot
                  mVecInitSetup[mCurrentTrial].Position,     // to this position
                  mVecInitSetup[mCurrentTrial].Orientation,  // with this orientation
                  false  // this is not a check, leave the robot there
                  )) {
    LOGERR << "Can't move robot in <" << mVecInitSetup[mCurrentTrial].Position << ">, <"
           << mVecInitSetup[mCurrentTrial].Orientation << ">" << std::endl;
  }
  mWaypoints.clear();
}

/* Fitness function */
Real NeatLoopFunctions::performance() {
  /* The performance is simply the distance of the robot to the origin */
  return mFootBot->GetEmbodiedEntity().GetOriginAnchor().Position.Length()
  // + (double)mController->penalty / 200
  ;
}

/* Class name, header filename */
REGISTER_LOOP_FUNCTIONS(NeatLoopFunctions, "neat_loop_functions")
