#include "neat_qtuser_functions.h"
#include "neat_loop_functions.h"

NeatQTUserFunctions::NeatQTUserFunctions()
    : mNeatLoopFunctions(
          dynamic_cast<NeatLoopFunctions&>(CSimulator::GetInstance().GetLoopFunctions())) {}

void NeatQTUserFunctions::DrawInWorld() {
  drawWaypoints(mNeatLoopFunctions.getWaypoints());
}

void NeatQTUserFunctions::drawWaypoints(const std::vector<CVector3>& waypoints) {

  /* Start drawing segments when you have at least two points */
  if (waypoints.size() > 1) {
    size_t unStart = 0;
    size_t unEnd = 1;
    while (unEnd < waypoints.size()) {
      DrawRay(CRay3(waypoints[unEnd], waypoints[unStart]), CColor::RED, 15.0f);
      ++unStart;
      ++unEnd;
    }
    // DrawText(waypoints.back(), "bot", CColor::GREEN);
  }
}

REGISTER_QTOPENGL_USER_FUNCTIONS(NeatQTUserFunctions, "neat_qtuser_functions")
