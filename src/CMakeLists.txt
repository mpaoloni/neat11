include_directories(${CMAKE_SOURCE_DIR}/src)

add_subdirectory(controller)
add_subdirectory(loop_functions)
add_subdirectory(embedding)
