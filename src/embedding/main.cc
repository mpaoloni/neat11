/*!
 * Entry point for the simulation through ARGoS.
 */

#include <iostream>
#include <iomanip>

#include <cstdio>
#include <cstring>

#include <getopt.h>  // Command line arguments

/* ARGoS-related headers */
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/simulator/loop_functions.h>

/* NEAT-related headers */
#include "lib/libneat/neat.h"
#include "lib/libneat/config.h"

#include "controller/neat_controller.h"
#include "loop_functions/neat_loop_functions.h"

/* Extra parameters / configuration */
#include "argos_config.h"

char usage[] =
    "\n"
    " Usage: %s <options> [savefile]\n"
    "\n"
    " Optional arguments:\n"
    "\n"
    "      -D, --draw-best        : Output best network topology in Gnuplot format\n"
    "\n"
    " Optional arguments (require a savefile to be specified):\n"
    "\n"
    "      -B, --best-only        : Run best individual only with GUI\n"
    "      -t, --trial N          : Specify number of trial (0-%u)\n"
    "\n"
    " Examples:\n"
    "\n"
    "   %s\n"
    "   %s neat.dat --best-only --trial 0\n\n"
    "%s";

Neat* neatHandler;
bool runBest = false;

std::string savefile;
unsigned trial = -1;
bool drawBest = false;
unsigned get_uint(char* in, unsigned* out);

using namespace argos;
int main(int argc, char** argv) {
  char* error = new char[256];
  error[0] = '\0';
  while (1) {
    int option_index = 0;
    static char short_options[] = "t:BDh";
    static struct option long_options[] = { { "best-only", 0, 0, 'B' },
                                            { "draw-best", 0, 0, 'D' },
                                            { "trial", 1, 0, 't' },
                                            { "help", 0, 0, 'h' },
                                            { 0, 0, 0, 0 } };

    int option = getopt_long(argc, argv, short_options, long_options, &option_index);
    if (option < 0)
      break;

    switch (option) {
    case 0:
      break;
    case 't':
      if (get_uint(optarg, &trial) != 0 || trial < 0 || (NUM_TRIALS - 1) < trial) {
        snprintf(error, 256, " [x] Bad trial -- %s\n\n", optarg);
        goto usage_err;
      }
      break;
    case 'B':
      runBest = true;
      break;
    case 'D':
      drawBest = true;
      break;
    case 'h':
      goto usage_err;
    case '?':
    default:
      fprintf(stderr, " Run \"%s --help\" for help.\n", argv[0]);
      return 1;
    }
  }

  if (argc - optind == 0 && (runBest == true || drawBest == true || trial != -1)) {
    snprintf(error, 256, " [x] Specified options require a savefile\n\n");
    goto usage_err;
  }
  else if (argc - optind == 1) {
    savefile = argv[optind];
  }
  else if (argc - optind > 1) {
    snprintf(error, 256, " [x] Too many arguments\n\n");

  usage_err:
    fprintf(stderr, usage, argv[0], NUM_TRIALS - 1, argv[0], argv[0], error);
    delete[] error;
    return 1;
  }
  delete[] error;
  if (trial == -1)
    trial = 0;
  std::cout << std::setprecision(4);

  /* Initialize Neat */
  Neat* pneat;
  !savefile.empty()
      ? (pneat = new Neat(savefile, SENSOR_LAYER_SIZE, OUTPUT_LAYER_SIZE))
      : (pneat = new Neat(NEAT_POPULATION, NEAT_GENERATIONS, SENSOR_LAYER_SIZE, OUTPUT_LAYER_SIZE));
  Neat& neat = *pneat;
  neatHandler = pneat;

  /* Algoritm parameters */
  neat.config.CROSSOVER = 0.75;
  neat.config.DROPOFF_AGE = 15;
  neat.config.C1 = 1.0;
  neat.config.C2 = 1.0;
  neat.config.C3 = 0.4;
  neat.config.DELTA_T = 2.0;
  neat.config.MUT_WEIGHT = 0.7;
  neat.config.MUT_ENABLE_LINK = 0.2;
  neat.config.MUT_DISABLE_LINK = 0.2;
  neat.config.MUT_CONNECT_BIAS = 0.2;
  neat.config.STEP_PERTURB = 0.333;
  neat.config.MUT_ADD_LINK = 0.4;
  neat.config.MUT_ADD_NODE = 0.07;
  neat.config.MUT_REMOVE_LINK = 0.2;
  neat.config.NO_LOOPS = true;
  neat.config.SPECIATION = 1;

  /* Initialize ARGoS */
  static argos::CSimulator& simulator = argos::CSimulator::GetInstance();
  if (runBest)
    simulator.SetExperimentFileName("neat-gui-trial.argos");
  else
    simulator.SetExperimentFileName("neat.argos");
  simulator.LoadExperiment();

  /* Get loop functions */
  static NeatLoopFunctions& loopFunctions =
      dynamic_cast<NeatLoopFunctions&>(simulator.GetLoopFunctions());

  const unsigned population = neat.config.POPULATION;
  size_t currentTrial = trial;
  do {

    /* Set current trial */
    loopFunctions.setTrial(currentTrial);

    /* This also calls CEvolutionLoopFunctions::Reset() */
    simulator.Reset();

    /* Run the experiment */
    simulator.Execute();

    /* Set fitness */
#define ARENA_SIZE (6.0 + 1.0 + 12.0)  // Convert to a maximization problem (account for penalty)
    double fitness = ARENA_SIZE - (double)loopFunctions.performance();
    std::cout << "Fitness: " << fitness << std::endl;

    if (runBest)  // Quit
      break;

    if (neat.getCycleCount() == population - 1 && !runBest) {  // Advance trial every each gen.
      argos::LOG << "[*] Incrementing trial ..\n";
      currentTrial++;
    }

    neat.setCurrentGenomeFitness(fitness);
    neat.advanceGenome();
  }
  while (!neat.done());

  if (!runBest)
    neat.writeSave("neat.dat");
  if (drawBest)
    neat.getBestGenome().paintNetwork("best", false);
  delete pneat;

  argos::LOG << "[!] Simulation is over\n";
  simulator.Destroy();

  return 0;
}

unsigned get_uint(char* in, unsigned* out) {
  unsigned long i, o = 0, len = strlen(in);
  for (i = 0; i < len; i++) {
    if ('0' <= *in && *in <= '9')
      o = o * 10 + *in - '0';
    else
      return 1;
    in++;
  };
  *out = o;
  return 0;
}
