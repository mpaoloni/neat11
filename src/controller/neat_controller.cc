#include "neat_controller.h"

static CRange<Real> NN_OUTPUT_RANGE(0.0f, 1.0f);
static CRange<Real> WHEEL_ACTUATION_RANGE(-10.0f, 10.0f);

NeatController::NeatController() {}
NeatController::~NeatController() {}

void NeatController::Init(TConfigurationNode& t_node) {
  try {  // Get sensor / actuator handles
    mRobotWheels = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
    mRobotLight  = GetSensor<CCI_FootBotLightSensor>("footbot_light");
#ifdef OBSTACLE_AVOIDANCE
    mRobotProximity = GetSensor<CCI_FootBotProximitySensor>("footbot_proximity");
    mRobotLed = GetActuator<CCI_LEDsActuator>("leds");
#endif
  } catch (CARGoSException& ex) {
    THROW_ARGOSEXCEPTION_NESTED("Error initializing sensors/actuators", ex);
  }
  // penalty = 0;
}

void NeatController::ControlStep() {

  /* Get sensory data */
  const CCI_FootBotLightSensor::TReadings& tLight = mRobotLight->GetReadings();
#ifdef OBSTACLE_AVOIDANCE
  const CCI_FootBotProximitySensor::TReadings& tProximity = mRobotProximity->GetReadings();
  bool turnOnLeds = false;
#endif

  /* Fill NN inputs from sensory data */
  for (size_t i = 0; i < tLight.size(); i++) {
    neatHandler->setInput(tLight[i].Value, i);
  }

#ifdef OBSTACLE_AVOIDANCE
  for (size_t i = 0; i < tProximity.size(); i++) {
    neatHandler->setInput(tProximity[i].Value, i + tLight.size());
    if (tProximity[i].Value > 0.9)
      turnOnLeds = true;
  }
  if (turnOnLeds)
    mRobotLed->SetAllColors(CColor::RED);
  else
    mRobotLed->SetAllColors(CColor::BLACK);
#endif

  /* Evaluate genome */
  if (runBest)
    neatHandler->evaluateBestGenome();
  else
    neatHandler->evaluateCurrentGenome();

  /*
   * Apply NN outputs to actuation
   * The NN outputs are in the range [0,1]
   * To allow for backtracking, we remap this range
   * into [-5:5] linearly.
   */
  NN_OUTPUT_RANGE.MapValueIntoRange(mLeftSpeed,                  // value to write
                                    neatHandler->getOutput(0),   // value to read
                                    WHEEL_ACTUATION_RANGE        // target range (here [-5:5])
                                    );
  NN_OUTPUT_RANGE.MapValueIntoRange(mRightSpeed,                 // value to write
                                    neatHandler->getOutput(1),   // value to read
                                    WHEEL_ACTUATION_RANGE        // target range (here [-5:5])
                                    );
  mRobotWheels->SetLinearVelocity(mLeftSpeed, mRightSpeed);
}

void NeatController::Reset() {
  // penalty = 0;
}

void NeatController::Destroy() {}

REGISTER_CONTROLLER(NeatController, "neat_controller")
