#ifndef NEAT_CONTROLLER
#define NEAT_CONTROLLER

/* Neat-related headers */
#include "lib/libneat/neat.h"

#include "../argos_config.h"

/* ARGoS-related headers */
#include <argos3/core/control_interface/ci_controller.h>
#include <argos3/plugins/robots/generic/control_interface/ci_differential_steering_actuator.h>
#include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_light_sensor.h>
#ifdef OBSTACLE_AVOIDANCE
# include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_proximity_sensor.h>
# include <argos3/plugins/robots/generic/control_interface/ci_leds_actuator.h>
#endif

using namespace argos;

/* A controller is simply an implementation of the CCI_Controller class */
class NeatController : public CCI_Controller {

public:
  NeatController();
  virtual ~NeatController();
  void Init(TConfigurationNode& t_node);
  void ControlStep();
  void Reset();
  void Destroy();

  // unsigned penalty;

private:
  /* Sensors / actuators */
  CCI_DifferentialSteeringActuator* mRobotWheels;
  CCI_FootBotLightSensor* mRobotLight;
#ifdef OBSTACLE_AVOIDANCE
  CCI_FootBotProximitySensor* mRobotProximity;
  CCI_LEDsActuator* mRobotLed;
#endif

  Real mLeftSpeed, mRightSpeed;  // Weels speed
};

#endif /* NEAT_CONTROLLER */
