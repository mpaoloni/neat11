#ifndef ARGOS_CONFIG_H
#define ARGOS_CONFIG_H

/*! Extra configuration */

#define NUM_TRIALS 24
#define NEAT_POPULATION 150
#define NEAT_GENERATIONS NUM_TRIALS

#define OBSTACLE_AVOIDANCE 1

extern Neat* neatHandler;
extern bool runBest;

#endif /* ARGOS_CONFIG_H */
