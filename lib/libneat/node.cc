/*!
 * A node is a neuron which can be of type INPUT, HIDDEN or OUPUT
 */

#include <math.h>
#include <vector>

#include "node.h"
#include "neat.h"

inline double linear(const double x) {
  return x;
}

inline double sigmoid(const double x) {
  return 1.0 / (1.0 + exp(-4.924273 * x));
}

inline double bipolar_sigmoid(const double x) {
  return 2.0 / (1.0 + exp(-4.924273 * x)) - 1.0;
}

// Needed for pair/tuple construction
Node::Node() {
  mNeuronId = NEURON_AMISS_ID;
  mValue = 0.0;
}

Node::Node(const neuron_id_t id) {
  const auto limit = Neat::getInputLayerSize() - 1;
  mNeuronId = id;
  if (id < limit) {
    mType = NodeType::SENSOR;
    mValue = Neat::getInput(id);
    fActivation = &linear;
  }
  else if (id == limit) {
    mType = NodeType::BIAS;
    mValue = 1.0;
    fActivation = &linear;
  }
  else if (id > limit && id <= limit + Neat::getOutputLayerSize()) {
    mType = NodeType::OUTPUT;
    mValue = 0.0;
    fActivation = &sigmoid;
  }
  else {
    mType = NodeType::HIDDEN;
    mValue = 0.0;
    fActivation = &NEURON_DEFAULT_ACTIVATION;
  }
}

double Node::activate(const double inputValue) {
  mValue = fActivation(inputValue);
  return mValue;
}

void Node::addIncoming(Gene gene) {
  mIncomingLinks.push_back(gene);
}

bool Node::isOfType(const NodeType type) {
  return mType == type;
}

neuron_id_t Node::getNodeId() { return mNeuronId; }
NodeType Node::getNodeType() { return mType; }
double Node::getValue() { return mValue; }
void Node::setValue(const double value) { mValue = value; }
