#ifndef GENE_H_
#define GENE_H_

#include <iostream>

#include "innovation.h"
#include "types.h"

class Gene {
  friend class Neat;
  friend class Species;
  friend class Genome;
  friend inline std::ostream& operator<<(std::ostream&, const Gene&);

public:
  Gene();  // Needed
  Gene(link_t link);
  Gene(link_t link, double weight);
  Gene(const Gene& gene);

  // Getters
  link_t getLink();
  neuron_id_t getInputNode();
  neuron_id_t getOutputNode();
  innovation_t getInnovation();
  double getWeight();
  bool isEnabled();

  // Setters
  bool setLink(link_t link);
  bool setInputNode(const neuron_id_t in);
  bool setOutputNode(const neuron_id_t out);
  void setWeight(const double weight);
  void setEnabled(const bool enable);

private:
  Gene(link_t link, double weight, innovation_t innovation);
  link_t mLink;
  double mWeight;
  bool mEnabled;
  innovation_t mInnovation;
};

std::ostream& operator<<(std::ostream& strm, const Gene& g) {
  return strm << "Gene(In: " << g.mLink.first << ", Out: " << g.mLink.second
              << ", Weight: " << g.mWeight << ", Innov: " << g.mInnovation << ", "
              << (g.mEnabled ? "Enabled" : "DISABLED") << ")";
}

#endif /* GENE_H_ */
