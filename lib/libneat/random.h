#ifndef RANDOM_H_
#define RANDOM_H_

#include <cfloat>  // DBL_MAX
#include <chrono>
#include <cmath>   // std::nextafter
#include <random>

/*!
 * Range [min, max] for double
 * Range [min, max) for integer
 */
class Random {
  std::mt19937 mRng;

public:
  Random() {
    auto duration = std::chrono::system_clock::now().time_since_epoch();
    auto seed = std::chrono::duration_cast<std::chrono::microseconds>(duration).count();
    mRng.seed(seed);
  }
  Random(unsigned int seed) { mRng.seed(seed); }

  double operator()(double max = 1.0) {
    std::uniform_real_distribution<double> dist(0.0, std::nextafter(max, DBL_MAX));
    return dist(mRng);
  }
  double operator()(double min, double max) {
    std::uniform_real_distribution<double> dist(min, std::nextafter(max, DBL_MAX));
    return dist(mRng);
  }

  int operator()(int max = std::mt19937::max()) {
    std::uniform_int_distribution<int> dist(0, max - 1);  // Otherwise [min, max]
    return dist(mRng);
  }
  int operator()(int min, int max) {
    std::uniform_int_distribution<int> dist(min, max - 1);  // Otherwise [min, max]
    return dist(mRng);
  }

  static Random& get() {
    static Random rng;
    return rng;
  }

  static Random& getWith(unsigned int seed) {
    static Random rng(seed);
    return rng;
  }
};

#define RNG (Random::get())

#endif /* RANDOM_H_ */
