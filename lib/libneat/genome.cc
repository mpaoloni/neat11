/*!
 * The Genome is a linear sequence of genes.
 */

#include <algorithm>
#include <fstream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>

#include "genome.h"
#include "neat.h"
#include "random.h"
#include "config.h"
#include "debug.h"

Genome::Genome() {
  mLastNeuronId = Neat::getInputLayerSize() + Neat::getOutputLayerSize() - 1;
  mFitness = 0;
  mAdjustedFitness = 0;
  mGlobalRank = 0;
}

Genome::Genome(unsigned numMutations) : Genome() {
  auto const sid = Neat::getInputLayerSize();
  for (size_t i = 0; i < Neat::getInputLayerSize() - 1; i++)
    for (size_t j = 0; j < Neat::getOutputLayerSize(); j++)
      addGene(Gene({ i, j + sid }, Neat::random(1.0) * 4.0 - 2.0, Neat::incInnovation()));
  while (numMutations--)
    mutate();
}

/*!
 * Add a gene to the genome
 * \return pointer to the added element if succeded, nullptr otherwise
 */
Gene* Genome::addGene(Gene gene) {
  auto p = mConnectGenes.insert({ gene.getInnovation(), gene });
  return p.second ? &((*(p.first)).second) : nullptr;
}

/*!
 * Check if genome contains link (connection gene)
 */
bool Genome::containsLink(const link_t link) {
  for (auto& g : mConnectGenes)
    if (g.second.getLink() == link)
      return true;
  return false;
}

/*!
 * Check if genome contains reverse link (connection gene)
 */
bool Genome::containsReverseLink(const link_t link) {
  for (auto& g : mConnectGenes)
    if (g.second.getLink().first  == link.second && g.second.getLink().second == link.first)
      return true;
  return false;
}

/*!
 * Get number of excess and disjoint genes
 * \param other Other genome to compare
 */
std::pair<unsigned, unsigned> Genome::excessDisjoint(Genome& other) {
  const auto min1 = mConnectGenes.begin()->first;
  const auto min2 = other.mConnectGenes.begin()->first;
  const auto max1 = mConnectGenes.empty() ? 0 : mConnectGenes.rbegin()->first;
  const auto max2 = other.mConnectGenes.empty() ? 0 : other.mConnectGenes.rbegin()->first;
  const auto upperlowerBound = std::max(min1, min2);
  const auto lowerUpperBound = std::min(max1, max2);

  unsigned excess = 0, disjoint = 0;
  for (auto const& g1 : mConnectGenes) {
    const auto inn1 = g1.first;
    if (inn1 < upperlowerBound || inn1 > lowerUpperBound) excess++;
    if (inn1 >= upperlowerBound && inn1 <= lowerUpperBound &&
        other.mConnectGenes.find(inn1) == other.mConnectGenes.end()) disjoint++;
  }
  for (auto const& g2 : other.mConnectGenes) {
    const auto inn2 = g2.first;
    if (inn2 < upperlowerBound || inn2 > lowerUpperBound) excess++;
    if (inn2 >= upperlowerBound && inn2 <= lowerUpperBound &&
        mConnectGenes.find(inn2) == mConnectGenes.end()) disjoint++;
  }

  return { excess, disjoint };
}

/*!
 * Get average weight difference
 * \param other Genome to compare
 */
double Genome::averageWeightDifference(Genome& other) {
  auto sum = 0, matches = 0;
  for (auto& g1 : mConnectGenes) {
    auto it = other.mConnectGenes.find(g1.first);
    if (it != other.mConnectGenes.end()) {
      sum += std::abs(g1.second.getWeight() - it->second.getWeight());
      matches++;
    }
  }
  return (matches == 0 ? 0.0 : ((double)sum / matches));
}

/*!
 * Get maximum number of genes
 * \param other Genome to compare
 */
unsigned Genome::numMaxGenes(const Genome& other) {
  return std::max(mConnectGenes.size(), (other.mConnectGenes).size());
}

/*!
 * Compatibility between two genomes
 * \param other Genome to compare
 * \return true if they belong to the same species, false otherwise
 */
bool Genome::isSameSpecies(Genome& other) {
  auto ed = excessDisjoint(other);
  auto d = ed.second;
  auto w = averageWeightDifference(other);
  auto n = numMaxGenes(other);
  n = n == 0 ? 1 : n;

  double delta;
  if (Neat::config.SPECIATION == 0) {
    auto e = ed.first;
    n = n < 20 ? 1 : n; // From publication
    delta = ((double) (Neat::config.C1 * e) / n) +
            ((double) (Neat::config.C2 * d) / n) +
            (Neat::config.C3 * w);
  }
  else {
    delta = d / n * Neat::config.C2 + w * Neat::config.C3;
  }
  return delta < Neat::config.DELTA_T;
}

/*!
 * Mutate randomly links weight
 */
void Genome::mutateWeight() {
  for (auto& g : mConnectGenes) {
    Gene* gene = &g.second;
    if (Neat::random(1.0) < Neat::config.MUT_WEIGHT) {  // Must mutate
      if (Neat::random(1.0) < 0.9)  // Must perturb or
        gene->setWeight(gene->getWeight() + Neat::random(1.0) * Neat::config.STEP_PERTURB * 2.0 -
                        Neat::config.STEP_PERTURB);
      else                          // assign random weight otherwise
        gene->setWeight(Neat::random(1.0) * 4.0 - 2.0);
    }
  }
}

/*!
 * Choose a random neuron in the network
 * \param input Allow input and disallow output neurons to be chosen, opposite case otherwise
 * \param prev Neuron chosen previously or #NEURON_AMISS_ID
 * \return neuron_id_t of the Neuron
 */
neuron_id_t Genome::randomNeuron(const bool input, const neuron_id_t prev) {
  std::unordered_set<neuron_id_t> candidates;

  if (input)   // Inputs (and Bias) are not chosen as destination for a link
    for (auto i = Neat::getInputLayerSize(); i--;)   // Add sensor neurons
      candidates.insert(i);

  if (!input)  // Outputs are not chosen as source for a link
    for (auto i = Neat::getOutputLayerSize(); i--;)  // Add output neurons
      candidates.insert(i + Neat::getInputLayerSize());

  for (auto& n : mConnectGenes) {
    const auto n1 = n.second.getInputNode();
    const auto n2 = n.second.getOutputNode();
    if (IS_HIDDEN(n1)) candidates.insert(n1);
    if (IS_HIDDEN(n2)) candidates.insert(n2);
  }

  if (prev != NEURON_AMISS_ID)
    candidates.erase(prev);  // Disallow (explicit) loop on node

  auto it = candidates.begin();
  std::advance(it, Neat::random((int)candidates.size()));
  return *it;
}

/*!
 * Choose a random neuron in the network as source for a link
 * \return neuron_id_t of the Neuron
 */
neuron_id_t Genome::randomNeuronSource(const neuron_id_t exclude) {
  return randomNeuron(true, exclude);
}

/*!
 * Choose a random neuron in the network as destination for a link
 * \return neuron_id_t of the Neuron
 */
neuron_id_t Genome::randomNeuronDestination(const neuron_id_t exclude) {
  return randomNeuron(false, exclude);
}

/*!
 * Add a new connection gene with random weight
 * \param addToBias force the connection to the bias node
 * \return true if added, false otherwise
 */
void Genome::mutateAddLink(const bool addToBias) {
  neuron_id_t n1 = randomNeuronSource(NEURON_BIAS_ID);
  neuron_id_t n2 = randomNeuronDestination(n1);

  if (addToBias) n1 = NEURON_BIAS_ID;
  link_t link = { n1, n2 };

  if (containsLink(link) || containsReverseLink(link))
    return;
  if (Neat::config.NO_LOOPS && loopPresent(link))
    return;

  addGene(Gene(link, Neat::random(Neat::config.WEIGHT_MIN, Neat::config.WEIGHT_MAX),
               Neat::incInnovation()));
}

/*!
 * Add a neuron to the hidden-layer by splitting a link into two
 * \return true if added, false otherwise
 */
void Genome::mutateAddNode() {
  if (mConnectGenes.size() == 0)
    return;
  auto it = mConnectGenes.begin();
  std::advance(it, Neat::random((int)mConnectGenes.size()));  // Get random gene
  Gene* chosen = &(it->second);
  if (!chosen->isEnabled())
    return;

  mLastNeuronId++;

  addGene(  // First link (with weight 1.0)
      Gene({ chosen->getInputNode(), mLastNeuronId }, 1.0, Neat::incInnovation()));

  addGene(  // Second link (same weight as the original)
      Gene({ mLastNeuronId, chosen->getOutputNode() }, chosen->getWeight(), Neat::incInnovation()));

  chosen->setEnabled(false);  // Disable old link
}

/*!
 * Remove a random link
 */
void Genome::mutateRemoveLink() {
  if (!mConnectGenes.empty()) {
    auto it = mConnectGenes.begin();
    std::advance(it, Neat::random((int)mConnectGenes.size()));
    mConnectGenes.erase(it);
  }
}

/*!
 * Enable or disable a link
 * \param enable Choose to enable (true), or disable (false)
 * \return true if operation was successful
 */
void Genome::mutateEnableDisableLink(const bool enable) {
  Gene** candidates = new Gene*[mConnectGenes.size()];
  int count = 0;
  for (auto& g : mConnectGenes) {
    Gene* gene = &g.second;
    if (gene->isEnabled() != enable)
      candidates[count++] = gene;
  }
  if (count) {
    Gene* chosen = candidates[Neat::random(count)];
    chosen->setEnabled(!chosen->isEnabled());
  }
  delete[] candidates;
}

void Genome::mutateAddGenericLink() { return mutateAddLink(false); }
void Genome::mutateAddBias() { return mutateAddLink(true); }
void Genome::mutateEnableLink() { return mutateEnableDisableLink(true); }
void Genome::mutateDisableLink() { return mutateEnableDisableLink(false); }

/*!
 * Perform the chosen mutation
 * \param chance Probability of the mutation to occur
 * \param f Function to execute
 * \return true if the mutation occurred, false otherwise
 */
void Genome::cycleChance(const double chance, GenomeMemberFn f) {
  double p = chance;
  while (p > 0) {  // In case I'll ever put a probability > 1.0
    if (Neat::random(1.0) < p)
      CALL_MEMBER_FN(*this, f)();
    p -= 1;
  }
}

/*!
 * Perform all the mutations
 */
void Genome::mutate() {
  const struct configuration* const local_conf = &Neat::config;
  cycleChance(local_conf->MUT_WEIGHT, &Genome::mutateWeight);
  cycleChance(local_conf->MUT_ADD_LINK, &Genome::mutateAddGenericLink);
  cycleChance(local_conf->MUT_CONNECT_BIAS, &Genome::mutateAddBias);
  cycleChance(local_conf->MUT_ADD_NODE, &Genome::mutateAddNode);
  cycleChance(local_conf->MUT_ENABLE_LINK, &Genome::mutateEnableLink);
  cycleChance(local_conf->MUT_DISABLE_LINK, &Genome::mutateDisableLink);
  cycleChance(local_conf->MUT_REMOVE_LINK, &Genome::mutateRemoveLink);
  buildNetwork();
}

/*!
 * Build network (phenotype of the genome) using topological sorting
 */
void Genome::buildNetwork() {
  mNodeGenes.clear();
  mNetwork.clear();

  // Insert inputs and bias (no incoming edges)
  for (auto i = Neat::getInputLayerSize(); i--; )
    mNodeGenes.insert({ i, Node(i) });

  std::stack<neuron_id_t> stack;
  std::unordered_set<neuron_id_t> visited;

  /* Input and bias nodes */
  for (auto i = Neat::getInputLayerSize(); i--; ) {
    stack.push(i);
    visited.insert(i);
  }

  // Using DFS
  while (!stack.empty()) {
    const auto v = stack.top();
    bool pushed = false;
    for (auto& g : mConnectGenes) {
      Gene& gene = g.second;
      if (gene.isEnabled() && gene.getInputNode() == v) {
        const auto oid = gene.getOutputNode();
        if (visited.find(oid) == visited.end()) {
          visited.insert(oid);
          stack.push(oid);
          pushed = true;
          break;
        }
      }
    }
    if (!pushed) {  // No outgoing edges or all vertices already visited
      stack.pop();
      if (IS_HIDDEN(v) || IS_OUTPUT(v)) {
        Node neuron(v);
        for (auto& g : mConnectGenes) {
          Gene& gene = g.second;
          if (gene.isEnabled() && gene.getOutputNode() == v)
            neuron.addIncoming(gene);
        }
        mNodeGenes.insert({ v, neuron });
        mNetwork.push_back(v);
      }
    }
  }
}

/*!
 * Evaluate network by activating the neurons
 */
void Genome::evaluateNetwork() {

  // Clear all previous values
  for (auto& n : mNodeGenes) {
    const auto id = n.first;
    if (IS_INPUT(id))
      n.second.setValue(Neat::getInput(id));  // Must update
    else if (IS_BIAS(id))
      n.second.setValue(1.0);
    else
      n.second.setValue(0.0);
  }

  auto it = mNetwork.rbegin();
  for ( ; it != mNetwork.rend(); it++) {
    double sum = 0;

    // For all incoming synapses of the neuron, multiply their weights for
    // the value of the source neuron
    for (auto& g : mNodeGenes[*it].mIncomingLinks) {
      const auto incoming = mNodeGenes.find(g.getInputNode());
      if (incoming != mNodeGenes.end())  // Path could be broken by disabled link (found using DFS)
                                         // even though the incoming link is enabled
        sum += g.getWeight() * ((*incoming).second).getValue();
    }
    if (!mNodeGenes[*it].mIncomingLinks.empty())  // Activate if it has incoming links
      mNodeGenes[*it].activate(sum);
  }

  const auto outputIdStart = Neat::getInputLayerSize();
  for (size_t i = 0; i < Neat::getOutputLayerSize(); i++) {
    const auto out = mNodeGenes.find(i + outputIdStart);
    if (out != mNodeGenes.end())  // Output node could be unreachable
      Neat::setOutput(((*out).second).getValue(), i);
    else
      Neat::setOutput(0, i);  // Unreachable, set to 0
    // DEBUG("Setting OUT[" << i << "] = " << Neat::getOutput(i));
  }
}

/*!
 * Draw the genome's graph structure (graphviz format)
 */
void Genome::paintNetwork(std::string fname, const bool drawDisabled) {
  std::ofstream f;
  f.open("neat_" + fname + ".txt", std::ios::out | std::ios::trunc);
  if (f.is_open()) {
    f << "digraph G {\n\trankdir=LR\n\tsplines=line"
         "\n\tnode [fixedsize=true,label=\"\"];"

         /* Input layer subgraph */
         "\n\tsubgraph inputs {"
         "\n\t\tcolor=white;"
         "\n\t\tnode [style=solid,color=blue4,shape=circle];"
         "\n\t\t";
    for (size_t i = 0; i < Neat::getInputLayerSize() - 1; i++)
      f << i << " [label=\"" << i << "\"] ";
    f << Neat::getInputLayerSize() - 1 << " [label=\"" << Neat::getInputLayerSize() - 1
      << "\",color=yellow4];";  // Bias node
    f << "\n\t\tlabel = \"Input layer\";";
    f << "\n\t}";

    /* Hidden layer subgraph */
    std::set<neuron_id_t> hidden;
    const auto max = Neat::getInputLayerSize() + Neat::getOutputLayerSize() - 1;
    for (auto& g : mConnectGenes) {
      if (g.second.getInputNode() > max)
        hidden.insert(g.second.getInputNode());
      if (g.second.getOutputNode() > max)
        hidden.insert(g.second.getOutputNode());
    }
    if (!hidden.empty()) {
      f << "\n\tsubgraph hidden {"
           "\n\t\tcolor=white;"
           "\n\t\tnode [style=solid,color=red4,shape=circle];"
           "\n\t\t";
      for (auto h : hidden)
        f << h << " [label=\"" << h << "\"] ";
      f << ';';
      f << "\n\t\tlabel = \"Hidden layer\";";
      f << "\n\t}";
    }

    /* Ouput layer subgraph */
    f << "\n\tsubgraph ouputs {"
         "\n\t\tcolor=white;"
         "\n\t\tnode [style=solid,color=seagreen2,shape=circle];"
         "\n\t\t";
    for (size_t i = Neat::getInputLayerSize();
         i < Neat::getInputLayerSize() + Neat::getOutputLayerSize(); i++)
      f << i << " [label=\"" << i << "\"] ";
    f << ';';
    f << "\n\t\tlabel = \"Ouput layer\";";
    f << "\n\t}\n";
    for (auto& g : mConnectGenes) {
      if (g.second.isEnabled() || drawDisabled) {
        f << '\t' << g.second.getInputNode() << " -> " << g.second.getOutputNode() << " [label=\""
          << g.second.getWeight() << "\"";
        if (!g.second.isEnabled())
          f << ",style=dotted];\n";
        else
          f << "];\n";
      }
    }
    f << '}';
    f.close();
  }
}

unsigned Genome::size() { return mConnectGenes.size(); }
fitness_t Genome::getFitness() { return mFitness; }
fitness_t Genome::getAdjustedFitness() { return mAdjustedFitness; };
unsigned Genome::getGlobalRank() { return mGlobalRank; }
neuron_id_t Genome::getLastNeuronId() { return mLastNeuronId; }

void Genome::setFitness(const fitness_t fitness) { mFitness = fitness; }
void Genome::setGlobalRank(unsigned rank) { mGlobalRank = rank; }
void Genome::setLastNeuronId(neuron_id_t id) { mLastNeuronId = id; }

/* Check (if inserting a link causes) loops */
bool Genome::loopPresent(const link_t link) {
  std::stack<neuron_id_t> stack;
  std::unordered_set<neuron_id_t> visited;

  // Must check if path exists between link.second-link.first
  const neuron_id_t s = link.second;
  stack.push(s);
  visited.insert(s);
  while (!stack.empty()) {
    neuron_id_t v = stack.top(); stack.pop();
    for (auto& g : mConnectGenes) {
      if (g.second.getInputNode() == v) {
        const neuron_id_t adj = g.second.getOutputNode();
        if (adj == s)
          return true;
        if (visited.find(adj) == visited.end()) {
          stack.push(adj);
          visited.insert(adj);
        }
      }
    }
  }
  return false;
}
