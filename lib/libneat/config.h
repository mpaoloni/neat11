#ifndef CONFIG_H_
#define CONFIG_H_

struct configuration {

  // Generic
  unsigned POPULATION;
  unsigned DROPOFF_AGE;       // Staleness counter
  unsigned NUM_GENERATIONS;
  double SURVIVAL_THRESHOLD;  // Percentage of genomes allowed to reproduce
  double WEIGHT_MIN;
  double WEIGHT_MAX;
  double CROSSOVER;

  // Speciation (delta = C1 * E / N + C2 * D / N + C3 * W)
  double C1;       // Excess coefficient
  double C2;       // Disjoint coefficient
  double C3;       // Weight difference coefficient
  double DELTA_T;  // Compatibility threshold
  double C_MOD;    // Compatibility modifier

  // Mutation rates
  double MUT_WEIGHT;
  double MUT_ADD_LINK;
  double MUT_ADD_NODE;
  double MUT_ENABLE_LINK;
  double MUT_DISABLE_LINK;
  double MUT_REMOVE_LINK;
  double MUT_CONNECT_BIAS;

  // Miscellaneous
  double STEP_PERTURB;
  unsigned SPECIATION;
  bool NO_LOOPS;
};

#endif /* CONFIG_H_ */
