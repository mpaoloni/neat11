#ifndef GENOME_H_
#define GENOME_H_

#include <iostream>
#include <map>
#include <unordered_map>

#include "gene.h"
#include "node.h"
#include "types.h"

class Genome {
  friend class Neat;
  friend class Species;
  friend inline std::ostream& operator<<(std::ostream&, const Genome&);

public:
  Genome();
  Genome(unsigned numMutations);
  Gene* addGene(const Gene gene);
  bool containsLink(const link_t link);
  bool containsReverseLink(const link_t link);
  bool isSameSpecies(Genome& other);
  void mutate();
  void buildNetwork();
  void evaluateNetwork();
  unsigned size();
  void paintNetwork(std::string fname, const bool drawDisabled);
  neuron_id_t getLastNeuronId();
  fitness_t getFitness();
  fitness_t getAdjustedFitness();
  unsigned getGlobalRank();
  void setLastNeuronId(neuron_id_t id);
  void setFitness(const fitness_t fitness);
  void setGlobalRank(unsigned rank);
  std::map<innovation_t, Gene> mConnectGenes;  // Connection genes (synapses)

private:
  std::unordered_map<neuron_id_t, Node> mNodeGenes;  // Lookup node-value table
  std::vector<neuron_id_t> mNetwork;  // Network (phenotype)
  neuron_id_t mLastNeuronId;  // Last neuron added (to the hidden layer)
  fitness_t mFitness;
  fitness_t mAdjustedFitness;
  unsigned mGlobalRank;

  bool loopPresent(const link_t link);  // Check wether adding a link creates a cycle
  std::pair<unsigned, unsigned> excessDisjoint(Genome& other);
  double averageWeightDifference(Genome& other);
  unsigned numMaxGenes(const Genome& other);
  void mutateWeight();
  void mutateAddLink(const bool addToBias);
  void mutateAddGenericLink();  // mutateAddLink(false)
  void mutateAddBias();         // mutateAddLink(true)
  void mutateAddNode();
  void mutateEnableDisableLink(const bool enable);
  void mutateEnableLink();   // mutateEnableDisableLink(true)
  void mutateDisableLink();  // mutateEnableDisableLink(false)
  void mutateRemoveLink();
  neuron_id_t randomNeuron(const bool input, const neuron_id_t exclude);
  neuron_id_t randomNeuronSource(const neuron_id_t exclude);       // randomNeuron(true)
  neuron_id_t randomNeuronDestination(const neuron_id_t exclude);  // randomNeuron(false)

  // See http://www.cs.technion.ac.il/users/yechiel/c++-faq/macro-for-ptr-to-memfn.html
  typedef void (Genome::*GenomeMemberFn)();
  #define CALL_MEMBER_FN(object, ptrToMember) ((object).*(ptrToMember))
  void cycleChance(const double chance, GenomeMemberFn f);
};

std::ostream& operator<<(std::ostream& strm, const Genome& genome) {
  strm << "Genome( ";
  for (const auto& gene : genome.mConnectGenes)
    strm << gene.second << " ";
  return strm << ")";
}

#endif /* GENOME_H_ */
