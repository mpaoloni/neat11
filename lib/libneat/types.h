#ifndef TYPES_H_
#define TYPES_H_

#include <utility>

typedef size_t innovation_t;
typedef size_t neuron_id_t;
typedef std::pair<neuron_id_t, neuron_id_t> link_t;
typedef double fitness_t;

#endif /* TYPES_H_ */
