#ifndef INNOVATION_H_
#define INNOVATION_H_

#include "types.h"

class Innovation {
  innovation_t mInnovation;

public:
  Innovation(innovation_t inn) { mInnovation = inn; }
  innovation_t incInnovation() { return ++mInnovation; }
  innovation_t getInnovation() { return mInnovation; }

  Innovation& operator++() {
    mInnovation++;
    return *this;
  }

  static Innovation& get(innovation_t inn = 1) {
    static Innovation innovation(inn);
    return innovation;
  }
};

#endif /* INNOVATION_H_ */
