/*!
 * A species is defined as a group of individuals with similar
 * biological features capable of interbreeding among themselves
 * but that are unable to breed with individuals outside their group.
 *   - Fitness Sharing and Niching Methods Revisited (B. Sareni, L. Krähenbühl)
 */
#include <vector>

#include "species.h"
#include "neat.h"
#include "config.h"

Species::Species() {
  mTopFitness = 0;
  mAvgFitness = 0;
  mStalenessCount = 0;
  mSpeciesId = Neat::lastSpeciesId;
  Neat::lastSpeciesId++;
}

Genome* Species::addGenome(Genome& genome) {
  mGenomes.push_back(genome);
  return &(mGenomes.back());
}

/*!
 * Perform crossover between to genomes
 * \param g1 First parent
 * \param g2 Second parent
 * \param child
 */
static void crossover(Genome* g1, Genome* g2, Genome* child) {
  if (g2->getFitness() > g2->getFitness()) {  // g1 highest in fitness
    auto temp = g1;
    g1 = g2;
    g2 = temp;
  }

  // Matching genes are chosen randomly between either parent
  // All excess or disjoint genes are always included from the fittest parent
  for (auto& g : g1->mConnectGenes) {
    auto it = g2->mConnectGenes.find(g.first);
    if (it != g2->mConnectGenes.end() && Neat::random(2) == 0 && it->second.isEnabled())
      child->addGene(g.second);  // Matching (or 50% chance)
    else
      child->addGene(g.second);  // Disjoint or excess (or 50% chance)
  }

  // TODO Add possible enable of a disabled gene

  child->setLastNeuronId(std::max(g1->getLastNeuronId(), g2->getLastNeuronId()));
}

/*!
 * Give breed two genomes
 * \param child
 */
void Species::breedChild(Genome* child) {
  if (Neat::random(1.0) < Neat::config.CROSSOVER) {
    Genome* g1 = &(mGenomes[Neat::random((int)mGenomes.size())]);
    Genome* g2 = &(mGenomes[Neat::random((int)mGenomes.size())]);
    crossover(g1, g2, child);
  } else {
    *child = mGenomes[Neat::random((int)mGenomes.size())];
  }
  child->mutate();
}

fitness_t Species::calculateAverageFitness() {
  unsigned sum = 0;
  for (auto& g : mGenomes)
    sum += g.getGlobalRank();
  mAvgFitness = sum / mGenomes.size();
  return mAvgFitness;
}

fitness_t Species::getTopFitness() { return mTopFitness; }
fitness_t Species::getAverageFitness() { return mAvgFitness; }
unsigned Species::getStalenessCount() { return mStalenessCount; }
unsigned Species::getNumGenomes() { return (unsigned)mGenomes.size(); }
