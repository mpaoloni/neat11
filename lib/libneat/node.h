#ifndef NODE_H_
#define NODE_H_

#include <vector>

#include "gene.h"
#include "types.h"

#define NEURON_AMISS_ID ((neuron_id_t)-1)
#define NEURON_BIAS_ID (Neat::getInputLayerSize() - 1)
#define NEURON_DEFAULT_ACTIVATION bipolar_sigmoid
#define NEURON_BIAS_VALUE 1.0

#define IS_INPUT(x)     ((x < Neat::getInputLayerSize() - 1))
#define IS_BIAS(x)      ((x == NEURON_BIAS_ID))
#define IS_INPUTBIAS(x) ((x < Neat::getInputLayerSize()))
#define IS_OUTPUT(x)    ((x >= Neat::getInputLayerSize() && \
                         x < Neat::getInputLayerSize() + Neat::getOutputLayerSize()))
#define IS_HIDDEN(x)    ((x >= Neat::getInputLayerSize() + Neat::getOutputLayerSize()))

// Hidden nodes in a minimal solution are required whether the input function
// is not linearly separable (i.e. XOR)

enum class NodeType : unsigned {
  SENSOR = 1 << 0,
  HIDDEN = 1 << 1,
  OUTPUT = 1 << 2,
  BIAS   = 1 << 3  // Node that can connect to any node other than inputs
};

class Gene;  // Forward declaration

class Node {
  friend class Genome;
  friend inline std::ostream& operator<<(std::ostream&, const Node&);

public:
  Node();  // Needed
  Node(const neuron_id_t id);
  double activate(const double inputValue);
  void addIncoming(Gene gene);
  bool isOfType(const NodeType type);
  neuron_id_t getNodeId();
  NodeType getNodeType();
  double getValue();
  void setValue(const double value);

private:
  neuron_id_t mNeuronId;
  NodeType mType;
  double mValue;
  double (*fActivation)(double x);  // Activation function
  std::vector<Gene> mIncomingLinks;
};

std::ostream& operator<<(std::ostream& strm, const Node& n) {
  return strm << "Node(Id: " << n.mNeuronId << ", Val: " << n.mValue
              << ", " << (n.mType == NodeType::HIDDEN ? "HIDDEN" :
              (n.mType == NodeType::SENSOR ? "sensor" :
              (n.mType == NodeType::BIAS ? "bias" :
              (n.mType == NodeType::OUTPUT ? "output" : "other")))) << ")";
}

#endif /* NODE_H_ */
