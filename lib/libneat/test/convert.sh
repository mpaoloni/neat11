#!/bin/bash

mkdir -p bin
mv neat_* bin/
for file in bin/neat_*
do
  dot -Tpng -O "$file"
done
