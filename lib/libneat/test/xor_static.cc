/*!
 * Test on learning how to perform a XOR.
 */

#include <iostream>
#include <iomanip>

/* NEAT-related headers */
#include "neat.h"
#include "config.h"
#include "types.h"
#include "random.h"

#define NEAT_POPULATION 150  // Number of genomes (networks)
#define NEAT_GENERATIONS 60  // Number of generations (turns)
#define SENSOR_LAYER_SIZE 2
#define OUTPUT_LAYER_SIZE 1
#define INPUT_COMBOS (1 << SENSOR_LAYER_SIZE)

double input[4][2] = { { 0.0, 0.0 }, { 0.0, 1.0 }, { 1.0, 0.0 }, { 1.0, 1.0 } };

double output[4] = { 0.0, 1.0, 1.0, 0.0 };

int main(int argc, char** argv) {

  if (argc > 2) {
    std::cerr << "[x] Unrecognized options, aborting ..\n";
    exit(2);
  }

  /* Initialize Neat */
  Neat* pneat;
  argv[1] ?
      (pneat = new Neat(argv[1], SENSOR_LAYER_SIZE, OUTPUT_LAYER_SIZE))
      :
      (pneat = new Neat(NEAT_POPULATION, NEAT_GENERATIONS, SENSOR_LAYER_SIZE, OUTPUT_LAYER_SIZE));
  Neat& neat = *pneat;

  neat.config.CROSSOVER = 0.75;
  neat.config.DROPOFF_AGE = 15;
  neat.config.C1 = 1.0;
  neat.config.C2 = 1.0;
  neat.config.C3 = 0.4;
  neat.config.DELTA_T = 2.0;
  neat.config.MUT_WEIGHT = 0.7;
  neat.config.MUT_ENABLE_LINK = 0.2;
  neat.config.MUT_DISABLE_LINK = 0.2;
  neat.config.MUT_CONNECT_BIAS = 0.2;
  neat.config.STEP_PERTURB = 0.333;
  neat.config.MUT_ADD_LINK = 0.2;
  neat.config.MUT_ADD_NODE = 0.09;
  neat.config.MUT_REMOVE_LINK = 0.2;
  neat.config.NO_LOOPS = true;
  neat.config.SPECIATION = 1;

  std::cout << std::fixed;
  std::cout << std::setprecision(2);

  std::cout << "neat.config.POPULATION: " << neat.config.POPULATION << '\n';
  std::cout << "neat.config.NUM_GENERATIONS: " << neat.config.NUM_GENERATIONS << '\n';
  std::cout << "neat.config.DROPOFF_AGE: " << neat.config.DROPOFF_AGE << '\n';
  std::cout << "neat.config.SURVIVAL_THRESHOLD: " << neat.config.SURVIVAL_THRESHOLD << '\n';
  std::cout << "neat.config.WEIGHT_MIN: " << neat.config.WEIGHT_MIN << '\n';
  std::cout << "neat.config.WEIGHT_MAX: " << neat.config.WEIGHT_MAX << '\n';
  std::cout << "neat.config.CROSSOVER: " << neat.config.CROSSOVER << '\n';
  std::cout << "neat.config.C1: " << neat.config.C1 << '\n';
  std::cout << "neat.config.C2: " << neat.config.C2 << '\n';
  std::cout << "neat.config.C3: " << neat.config.C3 << '\n';
  std::cout << "neat.config.DELTA_T: " << neat.config.DELTA_T << '\n';
  std::cout << "neat.config.MUT_WEIGHT: " << neat.config.MUT_WEIGHT << '\n';
  std::cout << "neat.config.MUT_ADD_LINK: " << neat.config.MUT_ADD_LINK << '\n';
  std::cout << "neat.config.MUT_ADD_NODE: " << neat.config.MUT_ADD_NODE << '\n';
  std::cout << "neat.config.MUT_ENABLE_LINK: " << neat.config.MUT_ENABLE_LINK << '\n';
  std::cout << "neat.config.MUT_DISABLE_LINK: " << neat.config.MUT_DISABLE_LINK << '\n';
  std::cout << "neat.config.MUT_REMOVE_LINK: " << neat.config.MUT_REMOVE_LINK << '\n';
  std::cout << "neat.config.MUT_CONNECT_BIAS: " << neat.config.MUT_CONNECT_BIAS << '\n';

  std::cout << std::setprecision(6);
  fitness_t maxFitness = 0;
  do {
    double fitness = 4.0;
    for (unsigned i = 0; i < INPUT_COMBOS; i++) {  // Test all possible input combos
      for (size_t j = 0; j < SENSOR_LAYER_SIZE; j++) {
        neat.setInput(input[i][j], j);
      }
      neat.evaluateCurrentGenome();
      fitness -= std::fabs(neat.getOutput(0) - output[i]);
    }
    if (fitness > maxFitness) {
      maxFitness = fitness;
      std::cout << "G" << neat.getNumGeneration() << ", R" << neat.getCycleCount()
                << " -> MaxFitness: " << fitness << '\n';
    }
    neat.setCurrentGenomeFitness(fitness);
    neat.advanceGenome();
  }
  while (!neat.done());

  // Genome& best = neat.stats.bestGenome;
  // best.paintNetwork(std::to_string(best.getFitness()), false);
  // neat.writeSave("neat.dat");
  delete pneat;

  return 0;
}
