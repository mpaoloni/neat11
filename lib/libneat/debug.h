#ifndef DEBUG_H_
#define DEBUG_H_

#include <iostream>

#if defined(SHOW_DEBUG)
#define DEBUG(x)                                                                                 \
  do {                                                                                           \
    std::cerr << "[\033[1;31mDEBUG\033[0m] "                                                     \
              << "\033[1;32m" << __FILE__ << ':' << __LINE__ << ':' << __func__ << "():\033[0m " \
              << "\033[1;35m" << x << "\033[0m" << std::endl;                                    \
  } while (0)
#else
#define DEBUG(x)
#endif

#endif /* DEBUG_H_ */
