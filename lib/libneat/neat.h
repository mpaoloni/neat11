#ifndef NEAT_H_
#define NEAT_H_

#include <memory>
#include <vector>

#include "genome.h"
#include "species.h"
#include "types.h"
#include "random.h"

#define NEAT_MAGIC "\x23\x47\x21\x70"
#define TAEN_CIGAM "\x70\x21\x47\x23"
#define NEAT_VER   "\x01\x00"
#define SAVE_NAME  "neat"
#define SAVE_EXT   "dat"

class Neat {
  friend inline std::ostream& operator<<(std::ostream& strm, const Neat& n);

public:
  Neat(const unsigned popolation, const unsigned numGenerations,
       const size_t inSize, const size_t outSize);
  Neat(std::string saveName, const size_t inSize, const size_t outSize);
  ~Neat();
  unsigned getNumGeneration();
  fitness_t getMaxFitnessEver();
  unsigned getNumSpecies();
  static innovation_t getInnovation();
  static innovation_t incInnovation();

  static double getInput(const size_t index);
  static double getOutput(const size_t index);
  static bool setInput(const double value, const size_t index);
  static bool setOutput(const double value, const size_t index);
  static size_t getInputLayerSize() { return mNumInputs; }
  static size_t getOutputLayerSize() { return mNumOutputs; }

  void setInputSize(const size_t size);
  void setOutputSize(const size_t size);
  void evaluateBestGenome();
  void evaluateCurrentGenome();
  void setCurrentGenomeFitness(const fitness_t fitness);
  fitness_t getCurrentGenomeFitness();
  void advanceGenome();
  Genome getCurrentGenomeCopy();
  bool done();
  size_t getCycleCount();
  Genome getBestGenome();

  bool writeSave(std::string fname);
  bool loadSave(std::string fname);

  struct speciesRecord {
    unsigned id;
    unsigned size;
    fitness_t fitness;
    fitness_t fitnessMax;
  };
  struct statistics {
    fitness_t* fitnessMax;
    fitness_t* fitnessAvg;
    unsigned* sizeBestGenome;
    unsigned* sizeEnabledBestGenome;
    std::vector<struct speciesRecord>* species;
    Genome bestGenome;
  };
  struct statistics stats;
  static unsigned lastSpeciesId;

  static struct configuration config;
  static Random random;

private:
  Species* addSpecies();
  Genome* addToCompatibleSpecies(Genome genome);
  void cullSpecies(const bool cutToOne);
  void removeStaleSpecies();
  void removeWeakSpecies();
  void rankGlobally();
  fitness_t getTotalAverageFitness();
  unsigned nextGeneration();

  std::vector<Species> mSpecies;
  unsigned mPopulation;
  unsigned mNumGeneration;
  unsigned mNumMaxGenerations;
  static Innovation innovation;
  std::string mFilename;
  size_t mGenomeIndex;
  size_t mSpeciesIndex;
  size_t mCycleCount;
  fitness_t mMaxFitnessEver;

  /* Inputs / Ouputs */
  std::vector<double> mInputs;
  std::vector<double> mOutputs;
  static size_t mNumInputs;
  static size_t mNumOutputs;
  static double* mRegisteredInputs;
  static double* mRegisteredOutputs;
};

std::ostream& operator<<(std::ostream& strm, const Neat& n) {
  return strm << "Neat(Pop: " << n.mPopulation << ", Gens: " << n.mNumMaxGenerations
              << ", In: " << n.mNumInputs - 1 << ", Out: " << n.mNumOutputs << ")";
}

#endif /* NEAT_H_ */
