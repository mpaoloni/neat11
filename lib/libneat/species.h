#ifndef SPECIES_H_
#define SPECIES_H_

#include <vector>

#include "genome.h"

class Species {
  friend class Neat;

public:
  Species();
  Genome* addGenome(Genome& genome);
  void breedChild(Genome* child);
  fitness_t calculateAverageFitness();
  fitness_t getTopFitness();
  fitness_t getAverageFitness();
  unsigned getStalenessCount();
  unsigned getNumGenomes();

private:
  std::vector<Genome> mGenomes;
  fitness_t mTopFitness;
  fitness_t mAvgFitness;
  unsigned mStalenessCount;
  unsigned mSpeciesId;
};

#endif /* SPECIES_H_ */
