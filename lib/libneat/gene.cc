/*!
 * The (connection) gene represents a single connection between two nodes
 * (neurons) in the network. It's the analogue of a synapse.
 */

#include "gene.h"
#include "types.h"

// Needed for pair/tuple construction
Gene::Gene() {
  mWeight = 1.0;
  mEnabled = false;
  mInnovation = 0;
}

Gene::Gene(link_t link) {
  mLink = link;
  mWeight = 1.0;
  mEnabled = true;
  mInnovation = 0;
}

Gene::Gene(link_t link, double weight) {
  mLink = link;
  mWeight = weight;
  mEnabled = true;
  mInnovation = 0;
}

Gene::Gene(link_t link, double weight, innovation_t innovation) : Gene(link, weight) {
  mInnovation = innovation;
}

Gene::Gene(const Gene& gene) {
  mLink = gene.mLink;
  mWeight = gene.mWeight;
  mEnabled = gene.mEnabled;
  mInnovation = gene.mInnovation;
}

// Getters
link_t Gene::getLink() { return mLink; }
neuron_id_t Gene::getInputNode() { return mLink.first; }
neuron_id_t Gene::getOutputNode() { return mLink.second; }
innovation_t Gene::getInnovation() { return mInnovation; }
double Gene::getWeight() { return mWeight; }
bool Gene::isEnabled() { return mEnabled; }

// Setters
bool Gene::setLink(link_t link) {
  mLink = link;
  return true;  // TODO add check
}

bool Gene::setInputNode(const neuron_id_t in) {
  mLink.first = in;
  return true;  // TODO add check
}

bool Gene::setOutputNode(const neuron_id_t out) {
  mLink.second = out;
  return true;  // TODO add check
}

void Gene::setWeight(const double weight) { mWeight = weight; }
void Gene::setEnabled(const bool enable) { mEnabled = enable; }
