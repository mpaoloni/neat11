/*!
 * Collection of species and main interface to the library.
 */

#include <algorithm>
#include <cmath>
#include <fstream>
#include <vector>

#include "neat.h"
#include "innovation.h"
#include "config.h"
#include "debug.h"

#define member_sizeof(type, member) sizeof(((type*)0)->member)

// Byte-order swap
template <class T>
void endswap(T* objp) {
  unsigned char* memp = reinterpret_cast<unsigned char*>(objp);
  std::reverse(memp, memp + sizeof(T));
}

// Static stuff
Random Neat::random = Random::get();
Innovation Neat::innovation = Innovation::get();
double* Neat::mRegisteredInputs = nullptr;
double* Neat::mRegisteredOutputs = nullptr;
size_t Neat::mNumInputs = 0;
size_t Neat::mNumOutputs = 0;
unsigned Neat::lastSpeciesId = 0;

// Base configuration
// (C++11 does not support C99 designated initializer list!)
struct configuration Neat::config = { 80, 15, 200, 0.2, -2.0, 2.0, 0.75, 1.0, 1.0, 0.25,
                                      0.8, 0.3, 0.7, 0.4, 0.2, 0.2, 0.4, 0.2, 0.4, 0.1, 1, false };

Neat::Neat(const unsigned population, const unsigned numGenerations,
           const size_t inSize, const size_t outSize) {

  mPopulation = population < 2 ? config.POPULATION : population;
  config.POPULATION = mPopulation;
  mNumMaxGenerations = numGenerations < 2 ? config.NUM_GENERATIONS : numGenerations;
  config.NUM_GENERATIONS = mNumMaxGenerations;
  DEBUG("Population set to \'" << mPopulation << "\', generations to \'"
      << mNumMaxGenerations << "\' ..");
  setInputSize(inSize);
  setOutputSize(outSize);
  innovation = Innovation::get(outSize);
  DEBUG("Starting innovation is \'" << innovation.getInnovation() << "\' ..");
  DEBUG("Spawning \'" << mPopulation << "\' genomes ..");
  for (auto i = population; i--;) {
    //Genome genome(1);
    Genome genome;
    genome.mutate();
    //DEBUG(genome);
    addToCompatibleSpecies(genome);
  }
  mNumGeneration = 0;
  mMaxFitnessEver = 0;
  mSpeciesIndex = 0;
  mGenomeIndex = 0;
  mCycleCount = 0;
  stats.fitnessMax = new double[mNumMaxGenerations];
  stats.fitnessAvg = new double[mNumMaxGenerations];
  stats.sizeBestGenome = new unsigned[mNumMaxGenerations];
  stats.sizeEnabledBestGenome = new unsigned[mNumMaxGenerations];
  stats.species = new std::vector<struct speciesRecord>[mNumMaxGenerations];
  DEBUG("Final innovation is \'" << innovation.getInnovation() << "\' ..");
  DEBUG("Unique number of species is \'" << mSpecies.size() << "\' ..");
}

Neat::Neat(std::string saveName, const size_t inSize, const size_t outSize) {
  if (!loadSave(saveName)) {
    std::cerr << "[x] Could not find \'" << saveName << "\', aborting ..\n";
    exit(1);
  }
  if (inSize != (mNumInputs - 1) || outSize != mNumOutputs) {
    std::cerr << "[x] Input or Output layer size does not match, aborting ..\n";
    exit(2);
  }
  mFilename = saveName;
}

Neat::~Neat() {
  delete[] stats.fitnessMax;
  delete[] stats.fitnessAvg;
  delete[] stats.sizeBestGenome;
  delete[] stats.sizeEnabledBestGenome;
  delete[] stats.species;
}

Species* Neat::addSpecies() {
  mSpecies.push_back(Species());
  return &(mSpecies.back());
}

Genome* Neat::addToCompatibleSpecies(Genome genome) {
  for (auto& s : mSpecies) {
    // Test first genome of each species only (TODO make it more random ?)
    if (s.mGenomes[0].isSameSpecies(genome))
      return s.addGenome(genome);
  }
  Species* s = addSpecies();
  return s->addGenome(genome);
}

fitness_t Neat::getTotalAverageFitness() {
  unsigned sum = 0;
  for (auto& s : mSpecies)
    sum += s.getAverageFitness();
  return sum;
}

unsigned Neat::nextGeneration() {
  cullSpecies(false);
  rankGlobally();
  removeStaleSpecies();
  rankGlobally();
  for (auto& s : mSpecies)
    s.calculateAverageFitness();
  removeWeakSpecies();
  const fitness_t totalFitness = getTotalAverageFitness();

  std::vector<Genome> offspring;
  offspring.reserve(mPopulation);

  for (auto& s : mSpecies) {
    const unsigned numToBreed = std::floor(s.getAverageFitness() / totalFitness * mPopulation) - 1;
    // DEBUG("Breeding \'" << numToBreed << "\' individuals ..");
    for (auto i = numToBreed; i--;) {
      Genome child;
      s.breedChild(&child);
      offspring.push_back(child);
    }
  }

  cullSpecies(true);  // Cull all but the top member of each species (elitism)

  while (offspring.size() + mSpecies.size() < mPopulation) {
    auto& species = mSpecies[Neat::random((int)mSpecies.size())];
    Genome child;
    species.breedChild(&child);
    offspring.push_back(child);
  }

  /* Copy offspring back */
  for (auto& child : offspring)
    addToCompatibleSpecies(child);

  return ++mNumGeneration;
}

void Neat::cullSpecies(const bool cutToOne) {
  for (auto& s : mSpecies) {
    // Order genomes by fitness (descending order)
    std::sort(s.mGenomes.begin(), s.mGenomes.end(),
              [](Genome& a, Genome& b) -> bool { return a.getFitness() > b.getFitness(); });

    unsigned remaining = s.mGenomes.size();
    const unsigned allowed = cutToOne ? 1 : std::ceil((double)remaining / 2);
    remaining -= allowed;
    while (remaining--) {  // Delete tail
      s.mGenomes.pop_back();
    }
  }
}

void Neat::rankGlobally() {
  std::vector<Genome*> allGenomes;
  for (auto& s : mSpecies)
    for (auto& g : s.mGenomes)
      allGenomes.push_back(&g);

  // Order genomes by fitness (ascending order)
  std::sort(allGenomes.begin(), allGenomes.end(),
            [](Genome* a, Genome* b) -> bool { return a->getFitness() < b->getFitness(); });

  for (unsigned rank = 0; rank < (unsigned)allGenomes.size(); rank++)
    allGenomes[rank]->setGlobalRank(rank);
}

void Neat::removeStaleSpecies() {
  for (auto s = mSpecies.begin(); s != mSpecies.end();) {
    std::sort(s->mGenomes.begin(), s->mGenomes.end(),
              [](Genome& a, Genome& b) -> bool { return a.getFitness() > b.getFitness(); });

    const fitness_t maxFitness = s->mGenomes.begin()->getFitness();
    if (maxFitness > s->mTopFitness) {
      s->mTopFitness = maxFitness;
      s->mStalenessCount = 0;
    } else {
      s->mStalenessCount++;
    }

    // Keep best one (elitism)
    if (s->getStalenessCount() >= Neat::config.DROPOFF_AGE && s->getTopFitness() < mMaxFitnessEver)
      s = mSpecies.erase(s);
    else
      s++;
  }
}

void Neat::removeWeakSpecies() {
  const fitness_t total = getTotalAverageFitness();
  for (auto s = mSpecies.begin(); s != mSpecies.end();) {
    if (std::floor((double)s->getAverageFitness() / total * mPopulation) == 0)
      s = mSpecies.erase(s);
    else
      s++;
  }
}

innovation_t Neat::getInnovation() { return innovation.getInnovation(); }
innovation_t Neat::incInnovation() { return innovation.incInnovation(); }
unsigned Neat::getNumGeneration() { return mNumGeneration; }
fitness_t Neat::getMaxFitnessEver() { return mMaxFitnessEver; }
unsigned Neat::getNumSpecies() { return (unsigned)mSpecies.size(); }

double Neat::getInput(const size_t index) { return mRegisteredInputs[index]; }
double Neat::getOutput(const size_t index) { return mRegisteredOutputs[index]; }

bool Neat::setInput(const double value, const size_t index) {
  if (index < mNumInputs) {
    // DEBUG("Setting input to " << index << " value " << value);
    mRegisteredInputs[index] = value;
    return true;
  }
  return false;
}

bool Neat::setOutput(const double value, const size_t index) {
  if (index < mNumOutputs) {
    // DEBUG("Setting output " << index << " value " << value);
    mRegisteredOutputs[index] = value;
    return true;
  }
  return false;
}

/*!
 * Set number of sensor nodes in the neural network
 */
void Neat::setInputSize(const size_t size) {
  #define BIAS_VALUE 1.0
  mNumInputs = size + 1;  // Account for BIAS node
  mInputs.reserve(mNumInputs);
  mRegisteredInputs = mInputs.data();
  for (size_t i = 0; i < size - 1; i++)
    mRegisteredInputs[i] = 0;
  mRegisteredInputs[size] = BIAS_VALUE;  // Set BIAS node value
  DEBUG("Registering \'" << size << "\' inputs (0 .. " << size - 1
                         << "), Bias node is number \'" << size << "\' ..");
}

/*!
 * Set number of output nodes in the neural network
 */
void Neat::setOutputSize(const size_t size) {
  mNumOutputs = size;
  mOutputs.reserve(size);
  mRegisteredOutputs = mOutputs.data();
  for (size_t i = 0; i < size; i++)
    mRegisteredOutputs[i] = 0;
  DEBUG("Registering \'" << size << "\' outputs ..");
}

/*!
 * Evaluate network of best genome
 */
void Neat::evaluateBestGenome() {
  stats.bestGenome.evaluateNetwork();
}

/*!
 * Evaluate network of current genome
 */
void Neat::evaluateCurrentGenome() {
  mSpecies[mSpeciesIndex].mGenomes[mGenomeIndex].evaluateNetwork();
}

/*!
 * Set current genome fitness
 */
void Neat::setCurrentGenomeFitness(const fitness_t fitness) {
  mSpecies[mSpeciesIndex].mGenomes[mGenomeIndex].setFitness(fitness);
  if (fitness > mMaxFitnessEver) {
    mMaxFitnessEver = fitness;
    stats.bestGenome = mSpecies[mSpeciesIndex].mGenomes[mGenomeIndex];  // Update best individual
  }
}

/*!
 * Get current genome fitness
 */
fitness_t Neat::getCurrentGenomeFitness() {
  return mSpecies[mSpeciesIndex].mGenomes[mGenomeIndex].getFitness();
}

/*!
 * Advance to the next genome in the population to evaluate
 */
void Neat::advanceGenome() {
  // DEBUG("Advance genome ..");
  stats.fitnessAvg[mNumGeneration] += getCurrentGenomeFitness();
  ++mCycleCount;
  if (++mGenomeIndex >= mSpecies[mSpeciesIndex].mGenomes.size()) {
    mGenomeIndex = 0;
    if (++mSpeciesIndex >= mSpecies.size()) {  // One entire cicle done
      mSpeciesIndex = 0;
      mCycleCount = 0;

      /* Stats gathering */
      stats.fitnessAvg[mNumGeneration] /= mPopulation;
      stats.fitnessMax[mNumGeneration] = mMaxFitnessEver;
      stats.sizeBestGenome[mNumGeneration] = stats.bestGenome.size();
      unsigned enabled = 0;
      for (auto& g : stats.bestGenome.mConnectGenes)
        if (g.second.isEnabled()) enabled++;
      stats.sizeEnabledBestGenome[mNumGeneration] = enabled;
      for (auto it = mSpecies.begin(); it < mSpecies.end(); it++) {
        fitness_t fitness = 0;
        fitness_t fitnessMax = 0;
        for (auto& g : it->mGenomes) {
          const auto f = g.getFitness();
          fitness += f;
          if (f > fitnessMax)
            fitnessMax = f;
        }
        fitness /= it->mGenomes.size();
        stats.species[mNumGeneration].push_back(
            { it->mSpeciesId, (unsigned)it->mGenomes.size(), fitness, fitnessMax });
      }

      nextGeneration();
    }
  }
}

Genome Neat::getCurrentGenomeCopy() {
  return mSpecies[mSpeciesIndex].mGenomes[mGenomeIndex];
}

Genome Neat::getBestGenome() {
  return stats.bestGenome;
}

/*!
 * Returns true if the algorithm has terminated
 */
bool Neat::done() {
  return (mNumGeneration == mNumMaxGenerations - 1) && (mCycleCount == mPopulation - 1);
}

size_t Neat::getCycleCount() {
  return mCycleCount;
}

bool Neat::writeSave(std::string fname) {
  std::ofstream outFile;
	outFile.open(fname, std::ios::out | std::ios::binary);
	if (!outFile.is_open())
    return false;

  { // Write header
    const uint32_t magic = *((uint32_t*)NEAT_MAGIC);
    const uint16_t version = *((uint16_t*)NEAT_VER);
    outFile.write(reinterpret_cast<const char*>(&magic), sizeof(magic));
    outFile.write(reinterpret_cast<const char*>(&version), sizeof(version));
  }

  { // Number of inputs and outputs
    outFile.write(reinterpret_cast<const char*>(&mNumInputs),  // Counts BIAS node too
        member_sizeof(Neat, mNumInputs));
    outFile.write(reinterpret_cast<const char*>(&mNumOutputs),
        member_sizeof(Neat, mNumOutputs));
  }

  { // Write configuration
    outFile.write(reinterpret_cast<const char*>(&config.POPULATION),
        member_sizeof(struct configuration, POPULATION));
    outFile.write(reinterpret_cast<const char*>(&config.DROPOFF_AGE),
        member_sizeof(struct configuration, DROPOFF_AGE));
    outFile.write(reinterpret_cast<const char*>(&config.NUM_GENERATIONS),
        member_sizeof(struct configuration, NUM_GENERATIONS));
    outFile.write(reinterpret_cast<const char*>(&config.SURVIVAL_THRESHOLD),
        member_sizeof(struct configuration, SURVIVAL_THRESHOLD));
    outFile.write(reinterpret_cast<const char*>(&config.WEIGHT_MIN),
        member_sizeof(struct configuration, WEIGHT_MIN));
    outFile.write(reinterpret_cast<const char*>(&config.WEIGHT_MAX),
        member_sizeof(struct configuration, WEIGHT_MAX));
    outFile.write(reinterpret_cast<const char*>(&config.CROSSOVER),
        member_sizeof(struct configuration, CROSSOVER));
    outFile.write(reinterpret_cast<const char*>(&config.C1),
        member_sizeof(struct configuration, C1));
    outFile.write(reinterpret_cast<const char*>(&config.C2),
        member_sizeof(struct configuration, C2));
    outFile.write(reinterpret_cast<const char*>(&config.C3),
        member_sizeof(struct configuration, C3));
    outFile.write(reinterpret_cast<const char*>(&config.DELTA_T),
        member_sizeof(struct configuration, DELTA_T));
    outFile.write(reinterpret_cast<const char*>(&config.C_MOD),
        member_sizeof(struct configuration, C_MOD));
    outFile.write(reinterpret_cast<const char*>(&config.MUT_WEIGHT),
        member_sizeof(struct configuration, MUT_WEIGHT));
    outFile.write(reinterpret_cast<const char*>(&config.MUT_ADD_LINK),
        member_sizeof(struct configuration, MUT_ADD_LINK));
    outFile.write(reinterpret_cast<const char*>(&config.MUT_ADD_NODE),
        member_sizeof(struct configuration, MUT_ADD_NODE));
    outFile.write(reinterpret_cast<const char*>(&config.MUT_ENABLE_LINK),
        member_sizeof(struct configuration, MUT_ENABLE_LINK));
    outFile.write(reinterpret_cast<const char*>(&config.MUT_DISABLE_LINK),
        member_sizeof(struct configuration, MUT_DISABLE_LINK));
    outFile.write(reinterpret_cast<const char*>(&config.MUT_REMOVE_LINK),
        member_sizeof(struct configuration, MUT_REMOVE_LINK));
    outFile.write(reinterpret_cast<const char*>(&config.MUT_CONNECT_BIAS),
        member_sizeof(struct configuration, MUT_CONNECT_BIAS));
    outFile.write(reinterpret_cast<const char*>(&config.STEP_PERTURB),
        member_sizeof(struct configuration, STEP_PERTURB));
    outFile.write(reinterpret_cast<const char*>(&config.SPECIATION),
        member_sizeof(struct configuration, SPECIATION));
    outFile.write(reinterpret_cast<const char*>(&config.NO_LOOPS),
        member_sizeof(struct configuration, NO_LOOPS));
  }

  { // Write indexes
  	outFile.write(reinterpret_cast<const char*>(&mNumMaxGenerations),
        member_sizeof(Neat, mNumMaxGenerations));
  	outFile.write(reinterpret_cast<const char*>(&mNumGeneration),
        member_sizeof(Neat, mNumGeneration));
    outFile.write(reinterpret_cast<const char*>(&mCycleCount),
        member_sizeof(Neat, mCycleCount));
    outFile.write(reinterpret_cast<const char*>(&mSpeciesIndex),
        member_sizeof(Neat, mSpeciesIndex));
    outFile.write(reinterpret_cast<const char*>(&mGenomeIndex),
        member_sizeof(Neat, mGenomeIndex));
    outFile.write(reinterpret_cast<const char*>(&mMaxFitnessEver),
        member_sizeof(Neat, mMaxFitnessEver));
    innovation_t inn = innovation.getInnovation();
    outFile.write(reinterpret_cast<const char*>(&inn), sizeof(inn));
  }

  { // Write population
    unsigned numSpecies = (unsigned) mSpecies.size();
    outFile.write(reinterpret_cast<const char*>(&numSpecies), sizeof(numSpecies));

    DEBUG("Writing \'" << numSpecies << "\' species to file ..");

    // Species
    for (unsigned i = 0; i < numSpecies; i++) {
  		Species& species = mSpecies[i];
  		auto topFitness = species.getTopFitness();
  		outFile.write(reinterpret_cast<const char*>(&topFitness),
          member_sizeof(Species, mTopFitness));
  		auto avgFitness = species.getAverageFitness();
  		outFile.write(reinterpret_cast<const char*>(&avgFitness),
          member_sizeof(Species, mAvgFitness));
  		auto staleness = species.getStalenessCount();
  		outFile.write(reinterpret_cast<const char*>(&staleness),
          member_sizeof(Species, mStalenessCount));
      unsigned numGenomes = (unsigned) species.getNumGenomes();
      outFile.write(reinterpret_cast<const char*>(&numGenomes), sizeof(numGenomes));

      DEBUG("Writing \'" << numGenomes << "\' genomes from species \'" << i << "\' ..");

      // Genomes
  		for (unsigned j = 0; j < numGenomes; j++) {
  			Genome& genome = species.mGenomes[j];
  			auto lastNeuronId = genome.getLastNeuronId();
  			outFile.write(reinterpret_cast<const char*>(&lastNeuronId),
            member_sizeof(Genome, mLastNeuronId));
        auto fitness = genome.getFitness();
      	outFile.write(reinterpret_cast<const char*>(&fitness),
            member_sizeof(Genome, mFitness));
        auto adjustedFitness = genome.getAdjustedFitness();
        outFile.write(reinterpret_cast<const char*>(&adjustedFitness),
            member_sizeof(Genome, mAdjustedFitness));
        auto globalRank = genome.getGlobalRank();
        outFile.write(reinterpret_cast<const char*>(&globalRank),
            member_sizeof(Genome, mGlobalRank));
        unsigned numGenes = (unsigned) genome.size();
        outFile.write(reinterpret_cast<const char*>(&numGenes), sizeof(numGenes));

        DEBUG("Writing \'" << numGenes << "\' genes from genome \'" << j << "\' ..");

        // Genes
        for (auto& g : genome.mConnectGenes) {
          Gene& gene = g.second;
    			neuron_id_t n1 = gene.getInputNode();
    			outFile.write(reinterpret_cast<const char*>(&n1), sizeof(n1));
          neuron_id_t n2 = gene.getOutputNode();
    			outFile.write(reinterpret_cast<const char*>(&n2), sizeof(n2));
          auto weight = gene.getWeight();
          outFile.write(reinterpret_cast<const char*>(&weight), member_sizeof(Gene, mWeight));
          auto enabled = gene.isEnabled();
          outFile.write(reinterpret_cast<const char*>(&enabled), member_sizeof(Gene, mEnabled));
          innovation_t inn = gene.getInnovation();
          outFile.write(reinterpret_cast<const char*>(&inn), sizeof(inn));
        }
  		}
    }
  }

  outFile.close();
  return true;
}

bool Neat::loadSave(std::string fname) {
  std::ifstream inFile;
  inFile.open(fname, std::ios::in | std::ios::binary);
  if (!inFile.is_open())
    return false;

  { // Read header
    uint32_t magic;
    uint16_t version;
    inFile.read((char*)&magic, sizeof(magic));
    if (magic != *((uint32_t*)NEAT_MAGIC)) {
      inFile.close();
      return false;
    }
    inFile.read((char*)&version, sizeof(version));
  }

  { // Read number of inputs and outputs
    size_t numInputs = 0;
    inFile.read((char*)&numInputs, member_sizeof(Neat, mNumInputs));
    numInputs--;
    size_t numOutputs = 0;
    inFile.read((char*)&numOutputs, member_sizeof(Neat, mNumOutputs));
    setInputSize(numInputs);
    setOutputSize(numOutputs);
  }

  { // Read configuration
    inFile.read((char*)&config.POPULATION,
        member_sizeof(struct configuration, POPULATION));
    mPopulation = config.POPULATION;
    inFile.read((char*)&config.DROPOFF_AGE,
        member_sizeof(struct configuration, DROPOFF_AGE));
    inFile.read((char*)&config.NUM_GENERATIONS,
        member_sizeof(struct configuration, NUM_GENERATIONS));
    inFile.read((char*)&config.SURVIVAL_THRESHOLD,
        member_sizeof(struct configuration, SURVIVAL_THRESHOLD));
    inFile.read((char*)&config.WEIGHT_MIN,
        member_sizeof(struct configuration, WEIGHT_MIN));
    inFile.read((char*)&config.WEIGHT_MAX,
        member_sizeof(struct configuration, WEIGHT_MAX));
    inFile.read((char*)&config.CROSSOVER,
        member_sizeof(struct configuration, CROSSOVER));
    inFile.read((char*)&config.C1,
        member_sizeof(struct configuration, C1));
    inFile.read((char*)&config.C2,
        member_sizeof(struct configuration, C2));
    inFile.read((char*)&config.C3,
        member_sizeof(struct configuration, C3));
    inFile.read((char*)&config.DELTA_T,
        member_sizeof(struct configuration, DELTA_T));
    inFile.read((char*)&config.C_MOD,
        member_sizeof(struct configuration, C_MOD));
    inFile.read((char*)&config.MUT_WEIGHT,
        member_sizeof(struct configuration, MUT_WEIGHT));
    inFile.read((char*)&config.MUT_ADD_LINK,
        member_sizeof(struct configuration, MUT_ADD_LINK));
    inFile.read((char*)&config.MUT_ADD_NODE,
        member_sizeof(struct configuration, MUT_ADD_NODE));
    inFile.read((char*)&config.MUT_ENABLE_LINK,
        member_sizeof(struct configuration, MUT_ENABLE_LINK));
    inFile.read((char*)&config.MUT_DISABLE_LINK,
        member_sizeof(struct configuration, MUT_DISABLE_LINK));
    inFile.read((char*)&config.MUT_REMOVE_LINK,
        member_sizeof(struct configuration, MUT_REMOVE_LINK));
    inFile.read((char*)&config.MUT_CONNECT_BIAS,
        member_sizeof(struct configuration, MUT_CONNECT_BIAS));
    inFile.read((char*)&config.STEP_PERTURB,
        member_sizeof(struct configuration, STEP_PERTURB));
    inFile.read((char*)&config.SPECIATION,
        member_sizeof(struct configuration, SPECIATION));
    inFile.read((char*)&config.NO_LOOPS,
        member_sizeof(struct configuration, NO_LOOPS));
  }

  { // Read indexes
    inFile.read((char*)&mNumMaxGenerations,
        member_sizeof(Neat, mNumMaxGenerations));
    inFile.read((char*)&mNumGeneration,
        member_sizeof(Neat, mNumGeneration));
    inFile.read((char*)&mCycleCount,
        member_sizeof(Neat, mCycleCount));
    inFile.read((char*)&mSpeciesIndex,
        member_sizeof(Neat, mSpeciesIndex));
    inFile.read((char*)&mGenomeIndex,
        member_sizeof(Neat, mGenomeIndex));
    inFile.read((char*)&mMaxFitnessEver,
        member_sizeof(Neat, mMaxFitnessEver));
    innovation_t inn;
    inFile.read((char*)&inn, sizeof(inn));
    innovation = Innovation::get(inn);
  }

  stats.fitnessMax = new double[mNumMaxGenerations];
  stats.fitnessAvg = new double[mNumMaxGenerations];
  stats.sizeBestGenome = new unsigned[mNumMaxGenerations];
  stats.sizeEnabledBestGenome = new unsigned[mNumMaxGenerations];
  stats.species = new std::vector<struct speciesRecord>[mNumMaxGenerations];

  if (done()) {  // Overwrite indexes if must start again
    mNumGeneration = 0;
    mCycleCount = 0;
    mSpeciesIndex = 0;
    mGenomeIndex = 0;
    mMaxFitnessEver = 0;
  }

  fitness_t bestGenomeFitness = 0;
  { // Read population
    unsigned numSpecies;
    inFile.read((char*)&numSpecies, sizeof(numSpecies));
    mSpecies.clear();
    mSpecies.reserve(numSpecies);

    DEBUG("Retrieving \'" << numSpecies << "\' species from previous save...");

    // Species
    for (unsigned i = 0; i < numSpecies; i++) {
      mSpecies.insert(mSpecies.begin() + i, Species());
      inFile.read((char*)&mSpecies[i].mTopFitness,
          member_sizeof(Species, mTopFitness));
      inFile.read((char*)&mSpecies[i].mAvgFitness,
          member_sizeof(Species, mAvgFitness));
      inFile.read((char*)&mSpecies[i].mStalenessCount,
          member_sizeof(Species, mStalenessCount));
      unsigned numGenomes;
      inFile.read((char*)&numGenomes, sizeof(numGenomes));
      mSpecies[i].mGenomes.clear();
      mSpecies[i].mGenomes.reserve(numGenomes);

      DEBUG("Retrieving \'" << numGenomes << "\' genomes from species \'" << i << "\' ..");

      // Genomes
      for (unsigned j = 0; j < numGenomes; j++) {
        mSpecies[i].mGenomes.insert(mSpecies[i].mGenomes.begin() + j, Genome());
        Genome* genome = &mSpecies[i].mGenomes[j];
        inFile.read((char*)&genome->mLastNeuronId,
            member_sizeof(Genome, mLastNeuronId));
        inFile.read((char*)&genome->mFitness,
            member_sizeof(Genome, mFitness));
        inFile.read((char*)&genome->mAdjustedFitness,
            member_sizeof(Genome, mAdjustedFitness));
        inFile.read((char*)&genome->mGlobalRank,
            member_sizeof(Genome, mGlobalRank));
        unsigned numGenes;
        inFile.read((char*)&numGenes, sizeof(numGenes));
        genome->mConnectGenes.clear();

        DEBUG("Retrieving \'" << numGenes << "\' genes from genome \'" << j << "\' ..");

        // Genes
        for (unsigned k = 0; k < numGenes; k++) {
          neuron_id_t n1, n2;
          double weight;
          bool enabled;
          innovation_t inn;
          inFile.read((char*)&n1, sizeof(n1));
          inFile.read((char*)&n2, sizeof(n2));
          inFile.read((char*)&weight, sizeof(weight));
          inFile.read((char*)&enabled, sizeof(enabled));
          inFile.read((char*)&inn, sizeof(inn));
          link_t link = { n1, n2 };
          Gene gene(link, weight, inn);
          gene.setEnabled(enabled);
          genome->mConnectGenes.insert({ inn, gene });
        }
        genome->buildNetwork();
        if (genome->getFitness() > bestGenomeFitness) {
          bestGenomeFitness = genome->getFitness();
          stats.bestGenome = *genome;
        }
      }
    }
  }
  DEBUG("Best genome has fitness \'" << stats.bestGenome.getFitness() << "\' ..");

  inFile.close();
  return true;
}
