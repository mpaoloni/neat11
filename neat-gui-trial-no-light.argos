<?xml version="1.0" ?>

<!-- *************************************************** -->
<!-- * A fully commented XML is diffusion_1.xml. Refer * -->
<!-- * to it to have full information about what       * -->
<!-- * these options mean.                             * -->
<!-- *************************************************** -->

<argos-configuration>

  <!-- ************************* -->
  <!-- * General configuration * -->
  <!-- ************************* -->
  <framework>
    <system threads="0" />
    <!-- Each experimental run is 90 seconds long -->
    <experiment length="90"
                ticks_per_second="10"
                random_seed="2170" />
  </framework>

  <!-- *************** -->
  <!-- * Controllers * -->
  <!-- *************** -->
  <controllers>

    <neat_controller id="fnn"
                     library="build/src/controller/libfootbot_neat">
      <actuators>
        <differential_steering implementation="default" />
      </actuators>
      <sensors>
        <footbot_light implementation="rot_z_only" show_rays="false" />
      </sensors>
      <params num_inputs="24"
              num_outputs="0" />
    </neat_controller>

  </controllers>

  <!-- ****************** -->
  <!-- * Loop functions * -->
  <!-- ****************** -->
  <loop_functions library="build/src/loop_functions/libneat_loop_functions"
                  label="neat_loop_functions"
                  trial="3" />

  <!-- *********************** -->
  <!-- * Arena configuration * -->
  <!-- *********************** -->
  <arena size="6, 6, 2" center="2.5,2.5,1">

    <!--
        Here we just put the static elements of the environment (the walls
        and the light).
        The dynamic ones, in this case the foot-bot, are placed by the
        loop functions at the beginning of each experimental run.
    -->

    <box id="wall_north" size="5,0.1,0.5" movable="false">
      <body position="2.5,5,0" orientation="0,0,0" />
    </box>

    <box id="wall_south" size="5,0.1,0.5" movable="false">
      <body position="2.5,0,0" orientation="0,0,0" />
    </box>

    <box id="wall_east" size="0.1,5,0.5" movable="false">
      <body position="0,2.5,0" orientation="0,0,0" />
    </box>

    <box id="wall_west" size="0.1,5,0.5" movable="false">
      <body position="5,2.5,0" orientation="0,0,0" />
    </box>

    <light id="light"
           position="0,0,1"
           orientation="0,0,0"
           color="blue"
           intensity="0"
           medium="leds" />

  </arena>

  <!-- ******************* -->
  <!-- * Physics engines * -->
  <!-- ******************* -->
  <physics_engines>
    <dynamics2d id="dyn2d" />
  </physics_engines>

  <!-- ********* -->
  <!-- * Media * -->
  <!-- ********* -->
  <media>
    <led id="leds" />
  </media>

  <!-- ****************** -->
  <!-- * Visualization * -->
  <!-- ****************** -->
  <visualization>
    <qt-opengl>
      <user_functions library="build/src/loop_functions/libneat_loop_functions"
                      label="neat_qtuser_functions" />
      <camera>
        <placement idx="0"
            position="5,2.5,4.8"
            look_at="2.5,2.5,0"
            lens_focal_length="24" />
        <placement idx="2"
            position="0.251795,1.03178,0.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="20" />
        <placement idx="3"
            position="4.85,4.85,0.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="20" />
      </camera>
    </qt-opengl>
  </visualization>

</argos-configuration>
