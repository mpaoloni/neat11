#!/bin/sh

rm -r -f build/
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
