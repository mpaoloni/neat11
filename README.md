# NeuroEvolution of Augmented Topologies (NEAT):
Personal implementation of the NeuroEvolution of Augmenting Topologies (NEAT) algorithm in C++11 and integration with [ARGoS](http://www.argos-sim.info/index.php) robot simulator. The library is just a personal project and shouldn't be intended for serious applications. It may contain bugs and other issues.

# Technical details:
Rank selection is used and network evaluation is made through topological sorting (RNNs are disabled). For the original implementation by Kenneth O. Stanley see the references.

# Dependencies:
Install [ARGoS](http://www.argos-sim.info/core.php) first with its dependencies. Then, be sure to have installed a C++11 capable compiler (GCC >= 4.8.1) and relative tools:
```
sudo apt-get update && sudo apt-get install build-essential cmake -y
```
The NEAT library can output the graph structure of a genome in the *graphviz format*, by calling the method `paintNetwork("filename", bool drawHiddenEdges)`. To convert (manually) the file into a visible image, however, the `graphviz` package is required:
```
sudo apt-get install graphviz -y
```
To convert the output file use:
```
dot -T<format> -O <file>
```

In the final image nodes have the following color scheme:

 * Input layer ![#1589F0](https://placehold.it/15/1589F0/000000?text=+)
 * Bias node ![#ffcc66](https://placehold.it/15/ffcc66/000000?text=+)
 * Hidden layer ![#f03c15](https://placehold.it/15/f03c15/000000?text=+)
 * Ouput layer ![#66ff66](https://placehold.it/15/66ff66/000000?text=+)

A *dotted line* is used to represent a link which is _**disabled**_ (when drawn).

# Building:
Every block of commands listed below is intended to be run from the top directory (`~/neat11`).

## Build and test the XOR example only
To build and test the XOR example there's no need to install the NEAT library:
```sh
cd lib/libneat/test
make
./bin/xor_static.out
```

## Build and install the NEAT library
```sh
cd lib/libneat
make shared
sudo make install
```

## Build the ARGoS project
Be sure to have installed the NEAT library beforehand:
```
./build.sh
```

## Playing with ARGoS
To train the neural network on the same arena, with same seed, run:
```
./train.sh <iterations>
```
This will create a savefile too. Example: `./train.sh 10`.

To train the network on the same arena but with a different seed each time, use `continuos_training.sh`:
```
./train.sh 1
./continuos_training.sh
```

The script `continuos_training.sh` is used just as an automatic way to change the seed after every training run. The script must be interrupted manually (Ctrl + C):

```bash
#!/bin/bash

while true; do
	COUNT=0
	while [ $COUNT -lt 1 ]; do
		let COUNT=COUNT+1
		./build/src/embedding/neat_phototaxis neat.dat
	done
	sed -i -E "s/(                random_seed=\")[0-9]+(\" \/>)/\1$(( ( RANDOM % 36500 )  + 1 ))\2/" neat.argos
	echo Sleeping ..
	sleep 2
done
```

## Test the results
Run:
```
./build/src/embedding/neat_phototaxis neat.dat --best-only -t <trial> [options]
```

The total number of trials, as well as other configurations, can be found in `src/argos_config.h`. For more information type:
```
./build/src/embedding/neat_phototaxis --help
```

## Change arena
The built binary expects two XML files: `neat.argos` for training and `neat-gui-trial.argos` for testing. Rename the desired files accordingly.

## Uninstall the NEAT library
The NEAT library and relative headers can be removed at anytime by running:
```sh
cd lib/libneat
sudo make uninstall
```

## Known issues
 * The number of generations must be > 1, otherwise the library will loop
 * Save files are endian-dependent

# References:
 * Website: http://www.cs.ucf.edu/~kstanley/neat.html
 * NEAT paper: http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf
### Original code by Kenneth O. Stanley:
 * http://nn.cs.utexas.edu/?neat-c
### The code is based on (although it differs from it):
 * MarI/O (Lua): https://www.youtube.com/watch?v=qv6UVOQ0F44
### Other useful sources:
 * FalpPy Bi/o (Python3): https://www.youtube.com/watch?v=H4WnRLEG73Q
 * NEATFlappyBird (Java): https://www.youtube.com/watch?v=zM4tBpE3Bnk
