Each genome was evaluated at the end of each run. Each run lasted a prefixed amount of seconds (eg. 90). To train the network for real obstacle avoidance (without the robot bumping into every obstacle on its way), each genome should be evaluated at every tick of every run (not just at the end). Also, because of the algorithm implementation, RNNs are disabled.

Tests:

* **test_1**: **phototaxis** only (max speed is 5)
* **test\_2**: first test with **light + proximity** sensors combined (spawn points are only on the RHS of the arena, 1 generation != 1 trial, max speed is 5)
* **test\_3**: same as *test\_2* except 1 generation == 1 trial, max speed is 10, leds turn on red when the robot is close to obstacles
* **test\_4**: same as *test\_3* except **spawn points are  in a circle all around the arena**, 15 degrees apart (population is 300 genomes)
* **test\_5**: same as *test\_4* except a penalty for passing very close to obstacles was introduced (proximity sensor read > 0.97, leds turn on yellow when applied)

The networks result of training may contain unused nodes and connections (due to the mutations that removes and disables genes). The bigger the network the easier for it to retain unused parts of the graph (due to mutation probability).
