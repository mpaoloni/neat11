<?xml version="1.0" ?>

<!-- *************************************************** -->
<!-- * A fully commented XML is diffusion_1.xml. Refer * -->
<!-- * to it to have full information about what       * -->
<!-- * these options mean.                             * -->
<!-- *************************************************** -->

<argos-configuration>

  <!-- ************************* -->
  <!-- * General configuration * -->
  <!-- ************************* -->
  <framework>
    <system threads="0" />
    <!-- Each experimental run is 200 seconds long -->
    <experiment length="200"
                ticks_per_second="10"
                random_seed="1337" />
  </framework>

  <!-- *************** -->
  <!-- * Controllers * -->
  <!-- *************** -->
  <controllers>

    <neat_controller id="fnn"
                     library="build/src/controller/libfootbot_neat">
      <actuators>
        <differential_steering implementation="default" />
        <leds implementation="default" medium="leds" />
      </actuators>
      <sensors>
        <footbot_light implementation="rot_z_only" show_rays="true" />
        <footbot_proximity implementation="default" show_rays="true" />
      </sensors>
      <params num_inputs="50"
              num_outputs="2" />
    </neat_controller>

  </controllers>

  <!-- ****************** -->
  <!-- * Loop functions * -->
  <!-- ****************** -->
  <loop_functions library="build/src/loop_functions/libneat_loop_functions"
                  label="neat_loop_functions" />

  <!-- *********************** -->
  <!-- * Arena configuration * -->
  <!-- *********************** -->
  <arena size="10, 10, 2" center="0,0,1">

    <!--
        Here we just put the static elements of the environment (the walls
        and the light).
        The dynamic ones, in this case the foot-bot, are placed by the
        loop functions at the beginning of each experimental run.
    -->

    <box id="wall_north" size="10,0.1,0.5" movable="false">
      <body position="0,4.8,0" orientation="0,0,0" />
    </box>
    <box id="wall_south" size="10,0.1,0.5" movable="false">
      <body position="0,-4.8,0" orientation="0,0,0" />
    </box>
    <box id="wall_east" size="0.1,10,0.5" movable="false">
      <body position="4.8,0,0" orientation="0,0,0" />
    </box>
    <box id="wall_west" size="0.1,10,0.5" movable="false">
      <body position="-4.8,0,0" orientation="0,0,0" />
    </box>

    <distribute>
      <position method="uniform" min="-5,-5,0" max="5,5,0" />
      <orientation method="uniform" min="0,0,0" max="360,0,0" />
      <entity quantity="30" max_trials="100">
        <box id="b" size="0.3,0.3,0.5" movable="false" />
      </entity>
    </distribute>

    <distribute>
      <position method="uniform" min="-5,-5,0" max="5,5,0" />
      <orientation method="constant" values="0,0,0" />
      <entity quantity="30" max_trials="100">
        <cylinder id="c" height="0.5" radius="0.15" movable="false" />
      </entity>
    </distribute>

    <light id="light"
           position="0,0,1"
           orientation="0,0,0"
           color="red"
           intensity="3"
           medium="leds" />

  </arena>

  <!-- ******************* -->
  <!-- * Physics engines * -->
  <!-- ******************* -->
  <physics_engines>
    <dynamics2d id="dyn2d" />
  </physics_engines>

  <!-- ********* -->
  <!-- * Media * -->
  <!-- ********* -->
  <media>
    <led id="leds" />
  </media>

  <!-- ****************** -->
  <!-- * Visualization * -->
  <!-- ****************** -->
  <visualization>
    <qt-opengl>
      <user_functions library="build/src/loop_functions/libneat_loop_functions"
                      label="neat_qtuser_functions" />
      <camera>
        <placement idx="0"
            position="5,2.5,4.8"
            look_at="2.5,2.5,0"
            lens_focal_length="28" />
        <placement idx="1"
            position="0.251795,1.03178,0.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="2"
            position="4.85,4.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="3"
            position="2.85,2.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="4"
            position="1.85,1.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="5"
            position="-1.85,4.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="6"
            position="-4.85,2.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="7"
            position="2.85,-4.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="8"
            position="-5.85,-4.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
        <placement idx="9"
            position="-1.85,-3.85,2.515305"
            look_at="0.233654,0.0876767,0.186165"
            lens_focal_length="24" />
      </camera>
    </qt-opengl>
  </visualization>

</argos-configuration>
