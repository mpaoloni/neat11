#!/bin/bash

echo Ready to execute $1 iterations ..
echo Executing round 0
./build/src/embedding/neat_phototaxis

COUNTER=1
while [  $COUNTER -lt $1 ]; do
    echo Executing round $COUNTER
    let COUNTER=COUNTER+1
    ./build/src/embedding/neat_phototaxis neat.dat
done
