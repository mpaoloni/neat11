#!/bin/bash

while true; do
	COUNT=0
	while [ $COUNT -lt 1 ]; do
		let COUNT=COUNT+1
		./build/src/embedding/neat_phototaxis neat.dat
	done
	sed -i -E "s/(                random_seed=\")[0-9]+(\" \/>)/\1$(( ( RANDOM % 36500 )  + 1 ))\2/" neat.argos
	echo Sleeping ..
	sleep 2
done
